import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:t/t.dart' as t;
import 'package:msgpack_dart/msgpack_dart.dart';
import 'package:threading/threading.dart';

void main(List<String> arguments) {
  
  /*
  var data = {
    "String": "Hello World",
    "b":false,
    "Integer": 42,
    "Double": 45.29,
    "Integer List": [1, 2, 3],
    "Integer Lissdasdst": ["1", "2", "3"],
    "Map": {
      1: 2,
      3: 4,
      "string" : DateTime.now().millisecondsSinceEpoch
    }
  };
  var p = serialize(data);
  print(p);
  print(p.length.toString());
  print(p.lengthInBytes.toString());
  var u = deserialize(p);
  print(u);
  print(u.length.toString());
  print(u["Integer"]);
  print('Hello world: ${t.calculate()}!');

  var packed = GetRoomData(5, 7, "test").pack();
  var c = GetRoomData.unpack(packed);
  print(c);
  */
  /*
  
  
  Communication._initializeUdpSocket();
  Communication._sendBroadcast();*/
}

enum MessageNum{
  Multicast,
  GetRoomData,
  JoinRoom,
  TimeSync,
  RoomStart,
  KickClient,
  RoomStatus,
  SongData,
  SongDataConfirm,
  PlayData,
  QuitRoom
}

enum MessageKeys{
  messageCode,
  roomName,
  amountOfMaxClients,
  currentClientsAmount
}

class Message {
  Message(this.messageCode);
  int messageCode;
  Uint8List pack() => serialize({MessageKeys.messageCode.index : messageCode});
  factory Message.unpack(Uint8List packed){
    var d = deserialize(packed);
    return Message(d[MessageKeys.messageCode.index]);
  }
}

class GetRoomData extends Message{
  GetRoomData(this.amountOfMaxClients, this.currentClientsAmount, this.roomName) : 
  super(MessageNum.GetRoomData.index);

  String roomName;
  int amountOfMaxClients;
  int currentClientsAmount;

  @override
  Uint8List pack() => serialize({MessageKeys.messageCode.index : super.messageCode, 
  MessageKeys.roomName.index : roomName, MessageKeys.amountOfMaxClients.index : amountOfMaxClients, 
  MessageKeys.currentClientsAmount.index : currentClientsAmount});

  @override
  factory GetRoomData.unpack(Uint8List packed){
    var d = deserialize(packed);
    return GetRoomData(d[MessageKeys.amountOfMaxClients.index], d[MessageKeys.currentClientsAmount.index], d[MessageKeys.roomName.index]);
  }
}

class ConstantValues{
  //static final Position oversightLocation = new Position(latitude: 32.083891, longitude: 34.806813);
  static final double initialMapZoom = 20;
  static final double roundRadius = 20;
  static final int sizeOfLengthString = 12;
  static final InternetAddress localIpAddress = InternetAddress.anyIPv4;
  static final InternetAddress udpMulticastGroup = new InternetAddress("239.10.10.100");
  static final int udpMulticastPort = 4545;
  static final int tcpManagerPort = 8081;
  static final int tcpP2pPort = 8080;
  static final String youMustInitializeAppConfigBeforeAccessingIt = "You must initialize AppConfig before you use it!";
  static final String tcpSocketInitTypeErr = "Type unknown";
  static final int maximumPriorityOperations = 5;
}

class Communication{
  static RawServerSocket _managerSocket;
  static RawServerSocket _songListenerSocket;
  static RawSocket _clientSocket;
  static RawDatagramSocket _udpSocket;
  static bool _shouldSendBroadcast = false;
  static List<InternetAddress> _roomsList;
  static Map<RawSocket, String> _usersList;
  static bool _shutDown = false;
  
    static void _sendBroadcast() async {
      if(_shouldSendBroadcast)
        return;
      _shouldSendBroadcast = true;
      List<int> data = utf8.encode('TEST');
      var thread = new Thread(() async {
        while(_shouldSendBroadcast && !_shutDown)
          _udpSocket.send(data, ConstantValues.udpMulticastGroup, ConstantValues.udpMulticastPort);
      });
      await thread.start();
    }
  
    static void _stopBrodcast() => _shouldSendBroadcast = false;
  
    static void refreshRoomsList() => _roomsList.clear();
  
    static void shutDown() => _shutDown = true;
  
    static void _initializeUdpSocket(){
      _roomsList = new List<InternetAddress>();
      RawDatagramSocket.bind(ConstantValues.localIpAddress, ConstantValues.udpMulticastPort).then((RawDatagramSocket s) {
        _udpSocket = s;
        s.joinMulticast(ConstantValues.udpMulticastGroup);
        s.listen((e) {
          Datagram dg = s.receive();
          if(_roomsList.where((t)=> t.address == dg.address.address) != null)
            _roomsList.add(dg.address);
          if (dg != null) {
            print("received ${dg.data}");
        }}).onError((e){print(e.toString());});
      });
    }
  
  }
  
  class Tuple2 {
}