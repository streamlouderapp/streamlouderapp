import 'dart:isolate';
import 'dart:typed_data';
import 'dart:io';

enum Language{
 HE,
 EN,
 ARB
}

class ConstTexts{
  String notice() => null;
  String thePermissionIsRequired() => null;
  String understood() => null;
  String tos() => null;
  String about() => null;
  String selectARoom() => null;
  String refresh() => null;
  String roomCreation() => null;
  String create() => null;
  String roomName() => null;
  String roomPassword() => null;
  String maxClientAmount() => null;
  String yourName() => null;
  String name() => null;
  String stringAreInvalid() => null;
  String clientAmountInvalid() => null;
}

class ConstTextsEN extends ConstTexts{
  @override
  String notice() =>  "Notice!";
  @override 
  String thePermissionIsRequired() => "Your permission is required.";
  @override
  String understood() => "understood.";
  @override
  String tos() => "Terms Of Service";
  @override
  String about() => "About";
  @override 
  String selectARoom() => "Select A Room";
  @override
  String refresh() => "Refresh";
  @override
  String roomCreation() => "Create A Room";
  @override
  String create() => "Create";
  @override
  String roomName() => "Room Name";
  @override
  String roomPassword() => "Room Password";
  @override
  String maxClientAmount() => "Amount Of Maximum Clients";
  @override
  String yourName() => "Your Name";
  @override
  String name() => "Name";
  @override
  String stringAreInvalid() => "Some fields are empty.";
  @override
  String clientAmountInvalid() => "The client amount should be >= " + ConstantValues.minimumMaxClients.toString() + " and <= " + 
  ConstantValues.maximumMaxClients.toString();
}

class ConstTextsHE extends ConstTexts{
  @override
  String notice() =>  "שים לב!";
  @override 
  String thePermissionIsRequired() => "ההרשאה שלך דרושה.";
  @override
  String understood() => "הבנתי.";
  @override
  String tos() => "תנאי שימוש";
  @override
  String about() => "מידע על האפליקציה";
  @override 
  String selectARoom() => "בחר חדר";
  @override
  String refresh() => "רענן";
  @override
  String roomCreation() => "צור חדר";
  @override
  String create() => "צור";
  @override
  String roomName() => "שם החדר";
  @override
  String roomPassword() => "סיסמת החדר";
  @override
  String maxClientAmount() => "כמות מקסימלית של משתמשים";
  @override
  String yourName() => "שמך";
  @override
  String name() => "שם";
  @override
  String stringAreInvalid() => "קיימים שדות ריקים.";
  @override
  String clientAmountInvalid() => "כמות המשתמשים צריכה להיות לפחות " + ConstantValues.minimumMaxClients.toString() + " ולכל היותר " + 
  ConstantValues.maximumMaxClients.toString();
}

class ConstantValues{
  //static final Position oversightLocation = new Position(latitude: 32.083891, longitude: 34.806813);
  static const double initialMapZoom = 20;
  static const double roundRadius = 20;
  static const int sizeOfLengthString = 12;
  static final InternetAddress localIpAddress = InternetAddress.anyIPv4;
  static final InternetAddress udpMulticastGroup = new InternetAddress("239.10.10.100");
  static const int udpMulticastPort = 4545;
  static const int tcpManagerPort = 8081;
  static const int tcpP2pPort = 8080;
  static const String youMustInitializeAppConfigBeforeAccessingIt = "You must initialize AppConfig before you use it!";
  static const String tcpSocketInitTypeErr = "Type unknown";
  static const int maximumPriorityOperations = 5;
  static final Duration socketTimerOperationDelay = new Duration(seconds: 2, milliseconds: 500);
  static const String messageTypeUndefined = "messageTypeUndefined";
  static const String roomNamePlaceholder = "Room Name";

  static const double applicationWidthMarginPercentage = 0.05;
  static const double applicationWidthPaddingPercentage = 0.1;
  static const double roomsListHeightPercentageOfScreen = 0.65;
  static const double roomsListWidthPercentageOfScreen = 0.8;
  static const double titleFontSize = 30;
  static const double defualtFontSize = 24;
  static const double circularProgressIndicatiorWidthPercentage = 0.1;
  static const double circularProgressIndicatiorHeightPercentage = 0.1;
  static const int minimumMaxClients = 2; //an admin and another person
  static const int maximumMaxClients = 10;
  static const double roundBorderWidth = 2;
  static const double roundBorderWidthForPopup = 4;
  static const int maxStringLength = 20;
  static const int maxIntStringLength = 2;
}

enum ErrorCodes{
  GeneralError,
  CantJoinRoomMaxClientsReached,
  PartyAlreadyStarted,
  NetworkError,
  WrongRoomPassword
}

enum TcpSocketType{
  ManagerListenerSocket,
  ClientSocket,
  SongListenerSocket,
  SongClientSocket
}

enum IsolateOperationType{
  SendToTcp,
  RecvFromTcp,
  SendToUdp,
  RecvFromUdp,
  InitManager,
  InitClient,
  InitP2pClient,
  InitP2pListener
}

enum IsolateDataType{
  BinaryMessage,
  ClientAddress,
  DestType
}

enum PopupMenuButtonOptions{
  TOS,
  About
}

void main(List<String> arguments) async{
  await Communicationisolate.callerCreateIsolate();
}




class Communicationisolate{
  //
  // The port of the new isolate
  // this port will be used to further
  // send messages to that isolate
  //
  static SendPort newIsolateSendPort;
  static SendPort newMainSendPort;

  //
  // Instance of the new Isolate
  //
  static Isolate newIsolate;

  //
  // Method that launches a new isolate
  // and proceeds with the initial
  // hand-shaking
  //
  static Future callerCreateIsolate() async {
    //
    // Local and temporary ReceivePort to retrieve
    // the new isolate's SendPort
    //
    var newMainRcvPort = ReceivePort();
    newMainRcvPort.listen((dynamic message){
      if(message is SendPort) {
        newIsolateSendPort = message;
        sendToIsolate(IsolateMessage(IsolateOperationType.SendToTcp));
      } 
      else{
        IsolateMessage incomingMessage = message as IsolateMessage;

        IsolateMessage ret = new IsolateMessage(IsolateOperationType.InitManager, content: {IsolateDataType.BinaryMessage : Uint8List(50), IsolateDataType.DestType : TcpSocketType.SongListenerSocket});

        //
        // Sends the outcome of the processing
        //
        sendToIsolate(message);
      }
    });
    //
    // Instantiate the new isolate
    //
    newIsolate = await Isolate.spawn(
        callbackFunction,
        newMainRcvPort.sendPort,
    );
    //newIsolateSendPort = await newMainRcvPort.first;
  }

  //
  // Routine to dispose an isolate
  //
  static void dispose(){
    newIsolate?.kill(priority: Isolate.immediate);
    newIsolate = null;
  }

  static void sendToIsolate(IsolateMessage message) async {
    newIsolateSendPort.send(message);
  }
  
  static void sendToMain(IsolateMessage message) async {
    newMainSendPort.send(message);
  }

  //
  // Extension of the callback function to process incoming messages
  //
  static void callbackFunction(SendPort callerSendPort){
    //
    // Instantiate a SendPort to receive message
    // from the caller
    //
    ReceivePort newIsolateReceivePort = ReceivePort();

    //
    // Provide the caller with the reference of THIS isolate's SendPort
    //
    callerSendPort.send(newIsolateReceivePort.sendPort);
    newMainSendPort = callerSendPort;

    //
    // Isolate main routine that listens to incoming messages,
    // processes it and provides an answer
    //
    newIsolateReceivePort.listen((dynamic message){
        IsolateMessage incomingMessage = message as IsolateMessage;

        IsolateMessage ret = new IsolateMessage(IsolateOperationType.InitManager, content: {IsolateDataType.BinaryMessage : Uint8List(50), IsolateDataType.DestType : TcpSocketType.ClientSocket});

        //
        // Sends the outcome of the processing
        //
        sendToMain(ret);
    });
  }
}

//
// Helper class
//
class IsolateMessage{
  IsolateMessage(this.isolateOperationType, {this.content});
  IsolateOperationType isolateOperationType;
  Map<IsolateDataType, dynamic> content;
}