import 'dart:async';

import 'package:flutter/material.dart';
import 'package:streamlouderapp/models/dataStructs.dart';
import 'definitions.dart';

class AppConfig {
  static setLanguage(Language language){
    _language = language;
  }
  static Brightness get brightness{
    return _brightness;
  }
  static Brightness _brightness;
  static ConstTexts _texts;
  static ConstTexts get localizedStrings{
    return _texts != null ? _texts : throw new Exception("Const texts not initialized");
  }
  static Language _language;
  static Language get language{
    return _language;
  }
  static MediaQueryData _mediaQueryData;
  static double _screenWidth;
  static double get screenWidth{
    throwExceptionIfNotInitialized();
    return _screenWidth;
  }
  static double _screenHeight;
  static double get screenHeight{
    throwExceptionIfNotInitialized();
    return _screenHeight;
  }
  static double _blockSizeHorizontal;
  static double get blockSizeHorizontal{
    throwExceptionIfNotInitialized();
    return _blockSizeHorizontal;
  }
  static double _blockSizeVertical;
  static double get blockSizeVertical{
    throwExceptionIfNotInitialized();
    return _blockSizeVertical;
  }
  static bool _initialized = false;
  static BuildContext _appContext;
  static BuildContext get appContext{
    throwExceptionIfNotInitialized();
    return _appContext;
  }
  static bool _rtl = false;

  static bool get rtl{
    throwExceptionIfNotInitialized();
    return _rtl;
  }

  static Timer timerRun;
  static DateTime _applicationTime;
  static DateTime get applicationTime {return _applicationTime;}
  static void setAppTime(TimeSyncResult result){
    timerRun?.cancel();
    _applicationTime = DateTime.fromMillisecondsSinceEpoch(
      result.timeSync.serverTime + (result.latency / 2).round(), isUtc: true);
    _updateTime();
  }
  static void _updateTime(){
    timerRun = Timer.periodic(ConstantValues.durationForDateTimeUpdate, (t){
      _applicationTime = _applicationTime.add(ConstantValues.durationForDateTimeUpdate);
      //TODO remove
      print(_applicationTime.toString());
    });
  }
  static List<TimeSyncResult> _timeSyncResults;
  static List<TimeSyncResult> get timeSyncResults{
    return _timeSyncResults;
  }


  static void init(BuildContext context, Language language){
    /*if(_initialized)
      return;*/
    _appContext = context;
    _rtl = (language == Language.HE);
    _language = language;
    switch(_language){
      case Language.EN:
        _texts = new ConstTextsEN();
        break;
      case Language.HE:
        _texts = new ConstTextsHE();
        break;
      default:
        break;
    }
    _mediaQueryData = MediaQuery.of(context);
    _screenWidth = _mediaQueryData.size.width;
    _screenHeight = _mediaQueryData.size.height;
    _blockSizeHorizontal = _screenWidth / 100;
    _blockSizeVertical = _screenHeight / 100;
    setAppTime(TimeSyncResult(TimeSync(0, DateTime.now().toUtc().millisecondsSinceEpoch), 0));
    _timeSyncResults = new List<TimeSyncResult>();
    _brightness = Theme.of(context).brightness == Brightness.light ? Brightness.dark : Brightness.light;//MediaQuery.of(context).platformBrightness;
    // we are toggling the brightness to the oppistite due to the brightness not being updated yet.
    ConstantValues.setSeconderyBackgroundColor(_brightness == Brightness.light);
    _initialized = true;
  }
  static void throwExceptionIfNotInitialized() => !_initialized ? throw new Exception(ConstantValues.youMustInitializeAppConfigBeforeAccessingIt) : null;
}