import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

enum Language{
 HE,
 EN
}

class ConstTexts{
  String notice() => null;
  String thePermissionIsRequired() => null;
  String understood() => null;
  String tos() => null;
  String about() => null;
  String selectARoom() => null;
  String refresh() => null;
  String roomCreation() => null;
  String create() => null;
  String roomName() => null;
  String roomPassword() => null;
  String maxClientAmount() => null;
  String yourName() => null;
  String name() => null;
  String stringAreInvalid() => null;
  String clientAmountInvalid() => null;
  String startParty() => null;
  String connectedUsers() => null;
  String kickUser() => null;
  String yes() => null;
  String no() => null;
  String join() => null;
  String partyAlreadyStarted() => null;
  String wrongPassword() => null;
  String maxClientsReached() => null;
  String nameTaken() => null;
  String wouldYouLikeToLeave() => null;
  String toggleBrightness() => null;
}

class ConstTextsEN extends ConstTexts{
  @override
  String notice() =>  "Notice!";
  @override 
  String thePermissionIsRequired() => "Your permission is required.";
  @override
  String understood() => "understood.";
  @override
  String tos() => "Terms Of Service";
  @override
  String about() => "About";
  @override 
  String selectARoom() => "Select A Room";
  @override
  String refresh() => "Refresh";
  @override
  String roomCreation() => "Create A Room";
  @override
  String create() => "Create";
  @override
  String roomName() => "Room Name";
  @override
  String roomPassword() => "Room Password";
  @override
  String maxClientAmount() => "Amount Of Maximum Clients";
  @override
  String yourName() => "Your Name";
  @override
  String name() => "Name";
  @override
  String stringAreInvalid() => "Some fields are empty.";
  @override
  String clientAmountInvalid() => "The client amount should be at least " + ConstantValues.minimumMaxClients.toString() + " and at max:  " + 
  ConstantValues.maximumMaxClients.toString();
  @override
  String startParty() => "Start Party!";
  @override
  String connectedUsers() => "Connected Users";
  @override
  String kickUser() => "Kick User";
  @override
  String yes() => "Yes";
  @override
  String no() => "No";
  @override
  String join() => "Join";
  @override
  String partyAlreadyStarted() => "Party Already Started :(";
  @override
  String wrongPassword() => "Wrong Password";
  @override
  String maxClientsReached() => "Max Clients Reached";
  @override
  String nameTaken() => "Name already in use!";
  @override
  String wouldYouLikeToLeave() => "Would You Like To Leave? :(";
  @override
  String toggleBrightness() => "Toggle Theme";
}

class ConstTextsHE extends ConstTexts{
  @override
  String notice() =>  "שים לב!";
  @override 
  String thePermissionIsRequired() => "ההרשאה שלך דרושה.";
  @override
  String understood() => "הבנתי.";
  @override
  String tos() => "תנאי שימוש";
  @override
  String about() => "מידע על האפליקציה";
  @override 
  String selectARoom() => "בחר חדר";
  @override
  String refresh() => "רענן";
  @override
  String roomCreation() => "צור חדר";
  @override
  String create() => "צור";
  @override
  String roomName() => "שם החדר";
  @override
  String roomPassword() => "סיסמת החדר";
  @override
  String maxClientAmount() => "כמות מקסימלית של משתמשים";
  @override
  String yourName() => "שמך";
  @override
  String name() => "שם";
  @override
  String stringAreInvalid() => "קיימים שדות ריקים.";
  @override
  String clientAmountInvalid() => "כמות המשתמשים צריכה להיות לפחות " + ConstantValues.minimumMaxClients.toString() + " ולכל היותר " + 
  ConstantValues.maximumMaxClients.toString();
  @override
  String startParty() => "תתחיל את החגיגה!";
  @override
  String connectedUsers() => "משתמשים מחוברים";
  @override
  String kickUser() => "הוצא את המשתמש";
  @override
  String yes() => "כן";
  @override
  String no() => "לא";
  @override
  String join() => "הצטרף";
  @override
  String partyAlreadyStarted() => "החגיגה התחילה בלעדיך :(";
  @override
  String wrongPassword() => "סיסמא שגויה";
  @override
  String maxClientsReached() => "כמות המשתמשים בחדר היא המקסימלית";
  @override
  String nameTaken() => "השם הזה כבר בשימוש!";
  @override
  String wouldYouLikeToLeave() => "אתה בטוח שאתה רוצה לעזוב ? :(";
  @override
  String toggleBrightness() => "שנה ערכת נושא";
}

class ConstantValues{
  static const double initialMapZoom = 20;
  static const double roundRadius = 20;
  static const int sizeOfLengthString = 12;
  static final InternetAddress localIpAddress = InternetAddress.anyIPv4;
  static final InternetAddress localLoopback = InternetAddress("127.0.0.1");
  static final InternetAddress udpMulticastGroup = new InternetAddress("239.10.10.100");
  static const int udpMulticastPort = 4545;
  static const int tcpManagerPort = 4546;
  static const int tcpP2pPort = 4547;
  static const String youMustInitializeAppConfigBeforeAccessingIt = "You must initialize AppConfig before you use it!";
  static const String tcpSocketInitTypeErr = "Type unknown";
  static const int maximumPriorityOperations = 5;
  static final Duration socketTimerOperationDelay = new Duration(seconds: 1);
  static const String messageTypeUndefined = "messageTypeUndefined";
  static const String roomNamePlaceholder = "Room Name";
  static const Color mainBackgroundColor = Color(0xFF64B5F6);
  static const MaterialColor mainColorSwatch = Colors.lightBlue;
  static Color get seconderyBackgroundColor{
    return _seconderyBackgroundColor;
  }
  static void setSeconderyBackgroundColor(bool light){
    _seconderyBackgroundColor = light ? seconderyBackgroundColorLight : seconderyBackgroundColorDark;
  }
  static Color _seconderyBackgroundColor;
  static const Color seconderyBackgroundColorDark = Color(0xff616161);
  static const Color seconderyBackgroundColorLight = Colors.white;
  static const double applicationWidthMarginPercentage = 0.05;
  static const double applicationWidthPaddingPercentage = 0.1;
  static const double roomsListHeightPercentageOfScreen = 0.65;
  static const double usersListHeightPercentageOfScreen = 0.45;
  static const double roomsListWidthPercentageOfScreen = 0.8;
  static const double titleFontSize = 30;
  static const double defualtFontSize = 24;
  static const double circularProgressIndicatiorWidthPercentage = 0.1;
  static const double circularProgressIndicatiorHeightPercentage = 0.1;
  static const int minimumMaxClients = 2; //an admin and another person
  static const int maximumMaxClients = 10;
  static const double roundBorderWidth = 2;
  static const double roundBorderWidthForPopup = 4;
  static const int maxStringLength = 20;
  static const int maxIntStringLength = 2;
  static final Color managerNameColor = Color(0xFFE0E0E0);
  static final int amountOfTimesForSync = 15;
  static final Duration durationTillRoomFinalStart = Duration(seconds: 15);
  static final Duration durationForDateTimeUpdate = Duration(milliseconds: 1);
  static final Duration durationSendStatusDelay = Duration(seconds: 5);
  static final double underlineHeight = 2;
  static final int indexForLanguageSubstring = 9;
}

enum ErrorCodes{
  GeneralError,
  CantJoinRoomMaxClientsReached,
  PartyAlreadyStarted,
  NetworkError,
  WrongRoomPassword,
  NoError,
  PlayerNameAlreadyTaken
}

enum TcpSocketType{
  ManagerListenerSocket,
  ClientSocket,
  SongListenerSocket,
  SongClientSocket
}

enum IsolateOperationType{
  SendToManagerFromClientSocket,
  IncomingFromManagerToClientSocket,
  SendToClientFromManagerSocket,
  IncomingFromClientToManagerSocket,
  SendToSongListenerFromClientSocket,
  IncomingFromSongClientToSongListenerSocket,
  InitUdp,
  InitManager,
  InitClient,
  InitP2pClient,
  InitP2pListener,
  StartBroadcast,
  StopBroadcast,
  CloseManager,
  CloseClient,
  CloseP2pListener,

  /// Map<String, String> -> names and IP addresses (we can't pass complex objects such as [InternetAddress] over the [Isolate] messaging API)
  GetRoomsList,

  /// Map<String,String> -> names and IP addresses (we can't pass complex objects such as [InternetAddress] over the [Isolate] messaging API)
  GetConnectedUsersList,

  RefreshRoomsList,
  ShutDown,
  KickClient,
  ClientQuit
}

enum IsolateDataType{
  Uint8listBinaryMessage,
  InternetAddressString,
  TcpDestTypeEnum,
  StringParam,
  MapParam
}


enum PopupMenuButtonOptions{
  TOS,
  About
}