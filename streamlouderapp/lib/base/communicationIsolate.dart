import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';
import 'dart:isolate';
import 'dart:typed_data';
import 'package:streamlouderapp/base/definitions.dart';
import 'package:streamlouderapp/base/general.dart' as general;
import 'package:tuple/tuple.dart';

/// static class to handle the second [Isolate] and the messaging between the [Isolate] and the main [Isolate]
class Communicationisolate{
  //
  // The port of the new isolate
  // this port will be used to further
  // send messages to that isolate
  //
  static SendPort newIsolateSendPort;
  static SendPort newMainSendPort;
  
  static ReceivePort newIsolateReceivePort;
  static ReceivePort newMainReceivePort;
  static Stream newMainReceivePortBroadcast;

  //
  // Instance of the new Isolate
  //
  static Isolate newIsolate;

  //
  // Method that launches a new isolate
  // and proceeds with the initial
  // hand-shaking
  //
  static Future callerCreateIsolate() async {
    //
    // Local and temporary ReceivePort to retrieve
    // the new isolate's SendPort
    //
    newMainReceivePort = ReceivePort();
    newMainReceivePortBroadcast = newMainReceivePort.asBroadcastStream();
    newMainReceivePortBroadcast.listen((dynamic message){
      if(message is SendPort) {
        newIsolateSendPort = message;
      } 
      else{
        IsolateMessage incomingMessage = message as IsolateMessage;
        try{
          switch(incomingMessage.isolateOperationType){
            case IsolateOperationType.IncomingFromManagerToClientSocket:
              general.Communication.handleIncomingPackedMsg(incomingMessage.content[IsolateDataType.Uint8listBinaryMessage],
              TcpSocketType.ClientSocket);
              break;
            case IsolateOperationType.IncomingFromClientToManagerSocket:
              general.Communication.handleIncomingPackedMsg(incomingMessage.content[IsolateDataType.Uint8listBinaryMessage],
              TcpSocketType.ManagerListenerSocket, socketAddr: incomingMessage.content[IsolateDataType.InternetAddressString]);
              break;
            case IsolateOperationType.IncomingFromSongClientToSongListenerSocket:
              general.Communication.handleIncomingPackedMsg(incomingMessage.content[IsolateDataType.Uint8listBinaryMessage],
              TcpSocketType.SongListenerSocket);
              break;
            case IsolateOperationType.GetRoomsList:
              if(general.Communication.roomsList == null)
                general.Communication.roomsList = new Map<String, String>();
              var updatedMap = incomingMessage.content[IsolateDataType.MapParam];
              general.Communication.roomsList.addAllDoNotOverwrite(updatedMap, onCompletion: general.Communication.roomListAlert);
              break;
            case IsolateOperationType.GetConnectedUsersList:
              var updatedMap = incomingMessage.content[IsolateDataType.MapParam];
              if(general.Communication.usersList == null)
                general.Communication.usersList = new Map<String, String>();
              general.Communication.usersList.addAllDoNotOverwrite(updatedMap, onCompletion: general.Communication.userListAlert);
              break;
            case IsolateOperationType.ShutDown:
              dispose();
              break;
            case IsolateOperationType.ClientQuit:
              general.Communication.usersList.remove(incomingMessage.content[IsolateDataType.InternetAddressString]);
              break;
            case IsolateOperationType.InitClient:
              general.Communication.alertClientSet();
              break;
            default:
              break;
          }
        }
        catch(e){print("Main Isolate msgListener: " + e.toString());}
      }
    });
    //
    // Instantiate the new isolate
    //
    newIsolate = await Isolate.spawn(
        callbackFunction,
        newMainReceivePort.sendPort,
    );
    newIsolateSendPort = await newMainReceivePortBroadcast.first;
  }

  //
  // Routine to dispose an isolate
  //
  static void dispose(){
    newMainReceivePort.close();
    newIsolate?.kill(priority: Isolate.immediate);
    newIsolate = null;
  }

  static void sendToIsolate(IsolateMessage message) async {
    newIsolateSendPort.send(message);
  }
  
  static void sendToMain(IsolateMessage message) async {
    newMainSendPort.send(message);
  }

  static void _alertRoomListUpdated(){
    sendToMain(IsolateMessage(IsolateOperationType.GetRoomsList, content: {IsolateDataType.MapParam : _Communication.roomsList}));
  }
  
  static void _alertClientConnected(){
    sendToMain(IsolateMessage(IsolateOperationType.InitClient));
  }

  //
  // Extension of the callback function to process incoming messages
  //
  static void callbackFunction(SendPort callerSendPort){
    //
    // Instantiate a SendPort to receive message
    // from the caller
    //
    newIsolateReceivePort = ReceivePort();

    //
    // Provide the caller with the reference of THIS isolate's SendPort
    //
    callerSendPort.send(newIsolateReceivePort.sendPort);
    newMainSendPort = callerSendPort;

    //
    // Isolate main routine that listens to incoming messages,
    // processes it and provides an answer
    //
    newIsolateReceivePort.listen((dynamic message)async{
        IsolateMessage incomingMessage = message as IsolateMessage;
        try
        {
          switch(incomingMessage.isolateOperationType){
          case IsolateOperationType.SendToManagerFromClientSocket:
            _Communication.sendMessage(incomingMessage.content[IsolateDataType.Uint8listBinaryMessage],
             TcpSocketType.ManagerListenerSocket);
            break;
          case IsolateOperationType.SendToClientFromManagerSocket:
            _Communication.sendMessage(incomingMessage.content[IsolateDataType.Uint8listBinaryMessage],
             TcpSocketType.ClientSocket, sock: _Communication._usersList.keys.where(
               (rawSock){return rawSock.remoteAddress.address == 
               incomingMessage.content[IsolateDataType.InternetAddressString];}).first);
            break;
          case IsolateOperationType.SendToSongListenerFromClientSocket:
            _Communication.sendMessage(incomingMessage.content[IsolateDataType.Uint8listBinaryMessage],
             TcpSocketType.SongListenerSocket, ip: incomingMessage.content[IsolateDataType.InternetAddressString]);
            break;
          case IsolateOperationType.InitUdp:
            _Communication.initializeUdpSocket();
            break;
          case IsolateOperationType.InitManager:
            await _Communication.initializeTcpSocket(TcpSocketType.ManagerListenerSocket);
            break;
          case IsolateOperationType.InitClient:
            await _Communication.initializeTcpSocket(TcpSocketType.ClientSocket, ip: InternetAddress(
              incomingMessage.content[IsolateDataType.InternetAddressString]));
            break;
          case IsolateOperationType.InitP2pClient:
            await _Communication.initializeTcpSocket(TcpSocketType.SongClientSocket, ip: InternetAddress(
              incomingMessage.content[IsolateDataType.InternetAddressString]));
            break;
          case IsolateOperationType.InitP2pListener:
            await _Communication.initializeTcpSocket(TcpSocketType.SongListenerSocket);
            break;
          case IsolateOperationType.StartBroadcast:
            _Communication.sendBroadcast(roomName: incomingMessage.content[IsolateDataType.StringParam]);
            break;
          case IsolateOperationType.StopBroadcast:
            _Communication.stopBrodcast();
            break;
          case IsolateOperationType.GetRoomsList:
            sendToMain(IsolateMessage(IsolateOperationType.GetRoomsList, content: 
            {IsolateDataType.MapParam : _Communication.roomsList}));
            break;
          case IsolateOperationType.GetConnectedUsersList:
            sendToMain(IsolateMessage(IsolateOperationType.GetConnectedUsersList, content: 
            {IsolateDataType.MapParam : _Communication.usersList}));
            break;
          case IsolateOperationType.RefreshRoomsList:
            _Communication.refreshRoomsList();
            break;
          case IsolateOperationType.ShutDown:
            _Communication.shutDown();
            sendToMain(IsolateMessage(IsolateOperationType.ShutDown)); // on success we send back the 
                                                                       // message so that we can kill 
                                                                       // the Isolate
            newIsolateReceivePort.close();                                                                       
            break;
          case IsolateOperationType.KickClient:
            var sock = _Communication._usersList.keys.where((k) => k.remoteAddress.address == 
            incomingMessage.content[IsolateDataType.InternetAddressString]).first;
            sock.close();
            _Communication._usersList.remove(sock);
            break;
          case IsolateOperationType.CloseManager:
            _Communication._managerSocket?.close();
            break;
          case IsolateOperationType.CloseClient:
            _Communication._clientSocket?.close();
            break;
          case IsolateOperationType.CloseP2pListener:
            _Communication._songListenerSocket?.close();
            break;
          default:
            break;  
          }
        }
        catch (e){print("Communication Isolate msgListener: " + e.toString());}
    });
    _Communication.queueHandlerStart();
  }
}

/// A static private Communication class to handle
/// the actual communication in the application (M2C, C2M, C2C)
/// it is controllerd by the [Communicationisolate] and is a "service" for the general [general.Communication] class.
class _Communication{
  static ServerSocket _managerSocket;
  static ServerSocket _songListenerSocket;
  static Socket _clientSocket;
  static RawDatagramSocket _udpSocket;
  static ListQueue<Tuple2<Socket, Uint8List>> _incomingMsgsForManager;
  static ListQueue<Tuple2<Socket, Uint8List>> _outgoingMsgsForManager;
  static ListQueue<Uint8List> _incomingMsgsForSongListener;
  static ListQueue<Tuple2<InternetAddress, Uint8List>> _outgoingMsgsForSongClient;
  static ListQueue<Uint8List> _incomingMsgsForClient;
  static ListQueue<Uint8List> _outgoingMsgsForClient;
  static bool _shouldSendBroadcast = false;
  static bool _handlerCompleted = true;
  static bool _udpHandlerCompleted = true;
  static Map<InternetAddress, String> _roomsList;
  static Map<String, String> get roomsList{
    return _roomsList.map((addr, name){return MapEntry(addr.address, name);});
  }
  static Map<Socket, String> _usersList;
  static Map<String, String> get usersList {
    return _usersList.map((sock, name){return MapEntry(sock.remoteAddress.address, name);});
  }
  static bool _shutDown = false;

  static void sendBroadcast({String roomName}){
    if(_shouldSendBroadcast)
      return;
    _shouldSendBroadcast = true;
    List<int> data = utf8.encode(roomName != null ? roomName : ConstantValues.roomNamePlaceholder);
    Timer.periodic(ConstantValues.socketTimerOperationDelay, (Timer t) async{
      if(_udpHandlerCompleted){
        _udpHandlerCompleted = false;
        if(!_shouldSendBroadcast || _shutDown)
          t.cancel();
        else
          _writeAllToUdpSocket(_udpSocket, data, ConstantValues.udpMulticastGroup, ConstantValues.udpMulticastPort);
        _udpHandlerCompleted = true;
      }
    });
  }

  static void stopBrodcast() => _shouldSendBroadcast = false;

  static void refreshRoomsList() => _roomsList.clear();

  static void shutDown() { 
    _shutDown = true;
    _managerSocket?.close();
    _songListenerSocket?.close();
    _udpSocket?.close();
    _clientSocket?.close();
  }

  static Future<Socket> initializeTcpSocket(TcpSocketType type, {InternetAddress ip}) async{
    bool managerSock;
    bool m2cClientSock;
    if((managerSock = (type == TcpSocketType.ManagerListenerSocket)) || type == TcpSocketType.SongListenerSocket){
      _initListener(managerSock);
      return null;
    }
    else if((m2cClientSock = (type == TcpSocketType.ClientSocket)) || type == TcpSocketType.SongClientSocket)
      return await _initClient(m2cClientSock, ip);
    return null;
  }


  static void sendMessage(Uint8List message, TcpSocketType dest, {InternetAddress ip, Socket sock}){
    if (dest == TcpSocketType.ClientSocket)
      _outgoingMsgsForManager.add(new Tuple2(sock, message));//_packedMsgToBytes(message)));
    else if (dest == TcpSocketType.ManagerListenerSocket)
      _outgoingMsgsForClient.add(message);//_packedMsgToBytes(message));
    else if (dest == TcpSocketType.SongListenerSocket)
      _outgoingMsgsForSongClient.add(new Tuple2(ip, message));//_packedMsgToBytes(message)));
  }

  static void queueHandlerStart() async{
    _shutDown = false;
    Timer.periodic(ConstantValues.socketTimerOperationDelay, (Timer t) async{
      int counter = 0;
      if(_handlerCompleted){
        _handlerCompleted = false;
        if(/*!_shouldSendBroadcast ||*/ _shutDown)
          t.cancel();
        else{
          while(_incomingMsgsForClient != null && _incomingMsgsForClient.isNotEmpty && counter < ConstantValues.maximumPriorityOperations)
          {
            Communicationisolate.sendToMain(IsolateMessage(IsolateOperationType.IncomingFromManagerToClientSocket
            , content: {IsolateDataType.Uint8listBinaryMessage:_incomingMsgsForClient.removeFirst()}));
            counter++;
          }
          counter = 0;
          while(_outgoingMsgsForManager != null &&_outgoingMsgsForManager.isNotEmpty && counter < ConstantValues.maximumPriorityOperations)
          {
            var curr = _outgoingMsgsForManager.removeFirst();
            _writeAllToTcpSocket(curr.item1, curr.item2);
            counter++;
          }
          counter = 0;
          //handle incoming from song sender and outgoing to song listener
          while(_incomingMsgsForSongListener != null &&_incomingMsgsForSongListener.isNotEmpty && counter < ConstantValues.maximumPriorityOperations)
          {
            Communicationisolate.sendToMain(IsolateMessage(IsolateOperationType.IncomingFromSongClientToSongListenerSocket
            , content: {IsolateDataType.Uint8listBinaryMessage:_incomingMsgsForSongListener.removeFirst()}));
            counter++;
          }
          counter = 0;
          while(_outgoingMsgsForSongClient != null &&_outgoingMsgsForSongClient.isNotEmpty && counter < ConstantValues.maximumPriorityOperations) {
            var curr = _outgoingMsgsForSongClient.removeFirst();
            var sock = await initializeTcpSocket(TcpSocketType.SongClientSocket, ip: curr.item1);
            _writeAllToTcpSocket(sock, curr.item2);
            sock.close();
            counter++;
          }
          counter = 0;
          //handle incoming from clients and outgoing to manager
          while(_incomingMsgsForManager != null &&_incomingMsgsForManager.isNotEmpty && counter < ConstantValues.maximumPriorityOperations){
            var curr = _incomingMsgsForManager.removeFirst();
            Communicationisolate.sendToMain(IsolateMessage(IsolateOperationType.IncomingFromClientToManagerSocket
            , content: {IsolateDataType.Uint8listBinaryMessage:curr.item2, 
            IsolateDataType.InternetAddressString: curr.item1.remoteAddress.address}));
            counter++;
          }
          counter = 0;
          while(_outgoingMsgsForClient != null &&_outgoingMsgsForClient.isNotEmpty && counter < ConstantValues.maximumPriorityOperations)
          {
            _writeAllToTcpSocket(_clientSocket, _outgoingMsgsForClient.removeFirst());
            counter++;
          }
          counter = 0;
        }
        _handlerCompleted = true;
      }
    });
  }
  static void dataHandler(data){
   print(new String.fromCharCodes(data).trim());
  }

  static void errorHandler(error, StackTrace trace){
   print(error);
  }

  static void _initListener(bool managerSock){
    if(managerSock){
        _managerSocket?.close();
    }
    else{
      _songListenerSocket?.close();
    }
    ServerSocket.bind(ConstantValues.localIpAddress, managerSock ? ConstantValues.tcpManagerPort : ConstantValues.tcpP2pPort)
    .then((ServerSocket serverSocket){
      if(managerSock){
        if(_usersList != null) _usersList?.clear();
        else _usersList = new Map<Socket, String>();
        _incomingMsgsForManager = new ListQueue();
        _outgoingMsgsForManager = new ListQueue();
        _managerSocket = serverSocket;
      }
      else{
        _incomingMsgsForSongListener = new ListQueue();
        _songListenerSocket = serverSocket;
      }
      serverSocket.listen((Socket socket){
        try{
          if(managerSock){
            _usersList.putIfAbsent(socket, (){return null;});
            Communicationisolate.sendToMain(IsolateMessage(IsolateOperationType.GetConnectedUsersList, content: {
              IsolateDataType.MapParam:usersList}));
          }
        }
        catch(e){print("While accepting client socket: " + e.toString()); socket.close();}
        var broad = socket.asBroadcastStream(); 
        broad.listen((data){
            managerSock ? _incomingMsgsForManager.add(new Tuple2(socket, data)) : 
            _incomingMsgsForSongListener.add(data);
          }, 
          onError: errorHandler, 
          onDone: (){socket.destroy();_usersList.remove(socket);}, 
          cancelOnError: false);
      });
    }, onError: (onError){print(onError.toString());});
  }

  /// if m2cClientSock is true, we will not return the rawSocket, but store it.
  static Future<Socket> _initClient(bool m2cClientSock, InternetAddress ip) async {
    if(m2cClientSock){
      await _clientSocket?.close();
    }
    var s = Socket.connect(ip, m2cClientSock ? ConstantValues.tcpManagerPort : ConstantValues.tcpP2pPort);
    s.catchError((onError){print(onError.toString());});
    if(!m2cClientSock)
      return await s;
    else{
      s.then((Socket clientSocket) async{
        _clientSocket = clientSocket;
        _outgoingMsgsForClient = new ListQueue();
        _incomingMsgsForClient = new ListQueue();
        _outgoingMsgsForSongClient = new ListQueue();
        clientSocket.listen((data){
          _incomingMsgsForClient.add(data);
          },
          onError: errorHandler, 
          onDone: (){clientSocket.destroy();}, 
          cancelOnError: false);
        Communicationisolate._alertClientConnected();
      }, onError: (onError){print(onError.toString());});
      return null;
    }
  }

  static void initializeUdpSocket(){
    if(_udpSocket != null)
      return;
    _roomsList = new Map<InternetAddress, String>();
    RawDatagramSocket.bind(ConstantValues.localIpAddress, ConstantValues.udpMulticastPort).then((RawDatagramSocket s) {
      _udpSocket = s;
      s.joinMulticast(ConstantValues.udpMulticastGroup);
      s.listen((e) {
        Datagram dg = _readAllFromUdpSocket(s);
        if(dg != null)
        {
          if(_roomsList.keys.length == 0 || _roomsList.keys.firstWhere((t)=> t.address == dg.address.address) == null){
            _roomsList.addAll({dg.address : utf8.decode(dg.data)});
            //_roomsList.putIfAbsent(dg.address, ()=> utf8.decode(dg.data));
            Communicationisolate._alertRoomListUpdated();
          }
          //print("received ${dg.data}");
        }
      }).onError((e){print(e.toString());});
    });
  }
  
  static void _writeAllToTcpSocket(Socket socket, Uint8List data){
    /*int amountWritten = 0;
    do
    {
      amountWritten += socket.write(data.sublist(amountWritten));
    }
    while(amountWritten < data.length);*/
    try{
    socket.add(data);//socket.write(data);
    }
    catch(e){print("While writing to tcpsocket: " + e.toString());}
  }
  
  static Datagram _readAllFromUdpSocket(RawDatagramSocket socket){
    return socket.receive();
  }
  
  static void _writeAllToUdpSocket(RawDatagramSocket socket, List<int> data, InternetAddress address, int port){
    int amountWritten = 0;
    do
    {
      amountWritten += socket.send(data.sublist(amountWritten), address, port);
    }
    while(amountWritten < data.length);
  }

/*
  static Uint8List _packedMsgToBytes(Uint8List packed){
    var lengthBytes = Uint8List.fromList(packed.lengthInBytes.toString().padLeft(ConstantValues.sizeOfLengthString).codeUnits);
    var finalList = List<int>();
    finalList.addAll(lengthBytes);
    finalList.addAll(packed);
    return Uint8List.fromList(finalList);
  }
  */
}

class IsolateMessage{
  /// Message to pass between [Isolate]s.
  IsolateMessage(this.isolateOperationType, {this.content});
  IsolateOperationType isolateOperationType;
  Map<IsolateDataType, dynamic> content;
}