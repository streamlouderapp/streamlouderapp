import "dart:async";
import 'dart:typed_data';
import 'package:streamlouderapp/models/clientRoomLandingPage.dart';
import 'package:streamlouderapp/models/managerRoomLandingPage.dart';
import 'package:flutter/material.dart';
import 'package:streamlouderapp/models/roomListScreenLogic.dart';
import 'appConfig.dart';
import 'definitions.dart';
import 'package:streamlouderapp/models/dataStructs.dart';
import 'package:streamlouderapp/models/joinRoom.dart' as joinRoomBackend;
import 'package:streamlouderapp/base/communicationIsolate.dart' as communicationIsolate;
import 'package:dynamic_theme/dynamic_theme.dart';

extension MapExt on Map {
  /// Method is an extention for [Map].
  /// Adds all the keys of "other", but does not overwrite the values of existing keys in source.
  /// basically just adds all of [other]'s "keys:values" but excludes them if they already exist in [source].
  /// 
  /// !- method assumes that [other] and [source] are not null -!
  /// 
  /// If [other] is null the method will not do anything.
  void addAllDoNotOverwrite<TK,TV>(Map<TK,TV> other, {Function onCompletion}){
    if(other != null){
      for(var key in other.keys){
        if(this.keys.length == 0 || this.keys.where((k) => k == key).length == 0)//if source is empty or if we cant find current 
        {                                                                             // key inside source.
          this[key] = other[key];
        }
      }
      if(onCompletion != null)
        onCompletion();
    }
  }
}

extension OrderedListExt on List<TimeSyncResult>{
  void addInOrder(TimeSyncResult timeSyncResult){
    this.add(timeSyncResult);
    this.sort((tsr1, tsr2) => tsr1.latency.compareTo(tsr2.latency));
  }
}

class ButtonsAndAction extends FlatButton{
  ///Providing a func is "optional", default will pop the latest in [Navigator]
  ButtonsAndAction(BuildContext context, String text, {Function func}) : super(child: new Text(text, textDirection: Helper.textDirection(),style: TextStyle(color: ConstantValues.mainBackgroundColor)
  ,), onPressed: func == null ? () {Navigator.of(context).pop();} : func);
}

class Helper{
  static void changeBrightness(BuildContext context) {
      DynamicTheme.of(context).setBrightness(Theme.of(context).brightness == Brightness.dark? Brightness.light: Brightness.dark);
    }

  static TextDirection textDirection() => AppConfig.rtl ? TextDirection.rtl : TextDirection.ltr;

  /// Used to push alerts of texts with a set of actions(buttons and actions) if wanted
  static Future pushDialog(BuildContext context, String title, String body, {List<ButtonsAndAction> actions, bool dismissable = true}) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return new WillPopScope(
        onWillPop: () async => dismissable,
        child:
        new AlertDialog(
          backgroundColor: ConstantValues.seconderyBackgroundColor,
          shape: new RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(ConstantValues.roundRadius)),
          side: BorderSide(color: ConstantValues.mainBackgroundColor, width: ConstantValues.roundBorderWidthForPopup)),
          title: new Container(child: new Text(title, textDirection: textDirection(), style: TextStyle(color: ConstantValues.mainBackgroundColor),), width: double.infinity,),
          content: new Container(child: SingleChildScrollView(child:
            new Text(body, textDirection: textDirection(), style: TextStyle(color: ConstantValues.mainBackgroundColor))), 
            width: double.infinity),
          actions: actions
        ));
      },
    );
  }
  
  /// Used to push popups just like screens with a set of actions(buttons and actions) if wanted. the body is the popup's content.
  /// The method makes sure to make the content scrollable.
  static Future pushPopup(BuildContext context, Widget body, {List<ButtonsAndAction> actions, bool dismissable = true}) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return new WillPopScope(
        onWillPop: () async => dismissable,
        child:
          new AlertDialog(
            backgroundColor: ConstantValues.seconderyBackgroundColor,
            shape: new RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(ConstantValues.roundRadius)),
            side: BorderSide(color: ConstantValues.mainBackgroundColor, width: ConstantValues.roundBorderWidthForPopup)),
            content:
                new Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, 
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Directionality(textDirection: textDirection(), child:
                      new Container(width: double.infinity, child: 
                        new Align(alignment: AlignmentDirectional.centerStart, child:
                          IconButton(icon: Icon(Icons.close, color: ConstantValues.mainBackgroundColor), onPressed: () => 
                          dismissable ? Navigator.pop(context) : null)))),
                    Flexible(child: 
                      SingleChildScrollView(child:
                        new Container(margin: new EdgeInsets.all(ConstantValues.applicationWidthMarginPercentage * 
                          AppConfig.screenWidth),
                          child: 
                            body))
                    )
                  ]),
            actions: actions
          ));
      },
    );
  }
}

/// A static Communication class to handle
/// all the communication in the application (M2C, C2M, C2C)
class Communication{
  static Map<String, String> _roomsList;
  /// map of addresses as keys and names as values.
  static Map<String, String> get roomsList{
    return _roomsList;
  }
  static set roomsList(Map val){
    _roomsList = val;
    roomListAlert();
  }

  static void roomListAlert(){
    RoomListScreenLogic.instance?.alertRoomsListUpdated();
  }
  
  static void userListAlert(){
    ManagerRoomLandingPage.currentInstance?.alertUsersListUpdated();
  }

  static Map<String, String> _usersList;
  /// map of addresses as keys and names as values. where the addreses are linkes to specific sockets inside the [communicationIsolate.Communicationisolate]'s private Communication class.
  static Map<String, String> get usersList{
    return _usersList;
  }
  static set usersList(Map<String, String> val){
    _usersList = val;
  }

  /// returns the actual users who are connected (have a name in the map as a value)
  static Map<String, String> get connectedUsers{
    if(usersList == null)
      return null;
    var ret = usersList.map((k,v)=> MapEntry(k,v));
    ret.removeWhere((k,v) => (v == null || v.isEmpty));
    return ret;
  }

  static void sendMessage(Message msg, TcpSocketType to, {String sockAddr}){
    switch (to) {
      case TcpSocketType.ClientSocket:
        communicationIsolate.Communicationisolate.sendToIsolate(communicationIsolate.IsolateMessage(IsolateOperationType.
        SendToClientFromManagerSocket, content: {IsolateDataType.Uint8listBinaryMessage : msg.pack(), 
        IsolateDataType.InternetAddressString : sockAddr}));
        break;
      case TcpSocketType.ManagerListenerSocket:
        communicationIsolate.Communicationisolate.sendToIsolate(communicationIsolate.IsolateMessage(IsolateOperationType.
        SendToManagerFromClientSocket, content: {IsolateDataType.Uint8listBinaryMessage : msg.pack()}));
        break;
      case TcpSocketType.SongListenerSocket:
        communicationIsolate.Communicationisolate.sendToIsolate(communicationIsolate.IsolateMessage(IsolateOperationType.
        SendToSongListenerFromClientSocket, content: {IsolateDataType.Uint8listBinaryMessage : msg.pack(), 
        IsolateDataType.InternetAddressString : sockAddr}));
        break;
      default:
        break;
    }
  }

  static void initializeTcpSocket(TcpSocketType type, {String ip}) async{
    bool managerSock;
    bool m2cClientSock;
    if((managerSock = (type == TcpSocketType.ManagerListenerSocket)) || type == TcpSocketType.SongListenerSocket){
      communicationIsolate.Communicationisolate.sendToIsolate(communicationIsolate.IsolateMessage(
        (managerSock ? IsolateOperationType.InitManager : IsolateOperationType.InitP2pListener)));
    }
    else if((m2cClientSock = (type == TcpSocketType.ClientSocket)) || type == TcpSocketType.SongClientSocket)
    {
      communicationIsolate.Communicationisolate.sendToIsolate(communicationIsolate.IsolateMessage((
        m2cClientSock ? IsolateOperationType.InitClient : IsolateOperationType.InitP2pClient), content: {
          IsolateDataType.InternetAddressString : ip}));
    }
  }

  static void initializeUdpSocket(){
    communicationIsolate.Communicationisolate.sendToIsolate(communicationIsolate.IsolateMessage(
      IsolateOperationType.InitUdp
    ));
  }

  static void sendBroadcast({String roomName = ConstantValues.roomNamePlaceholder}){
    communicationIsolate.Communicationisolate.sendToIsolate(communicationIsolate.IsolateMessage(
      IsolateOperationType.StartBroadcast, content: {IsolateDataType.StringParam : roomName}
    ));
  }
  
  static void stopBroadcast(){
    communicationIsolate.Communicationisolate.sendToIsolate(communicationIsolate.IsolateMessage(IsolateOperationType.StopBroadcast));
  }

  /// will request rooms list and at some point will update the [Communication.roomsList]
  static void requestRoomsList(){
    communicationIsolate.Communicationisolate.sendToIsolate(communicationIsolate.IsolateMessage(IsolateOperationType.GetRoomsList));
  }
  
  /// will request users list and at some point will update the [Communication.usersList]
  static void requestUsersList(){
    communicationIsolate.Communicationisolate.sendToIsolate(communicationIsolate.IsolateMessage(IsolateOperationType.
    GetConnectedUsersList));
  }

  static void requestRefreshRoomsList(){
    roomsList?.clear();
    communicationIsolate.Communicationisolate.sendToIsolate(communicationIsolate.IsolateMessage(IsolateOperationType.
    RefreshRoomsList));
  }

  /// will request shut down, once done we will receive the [IsolateOperationType.ShutDown] msg and we will call
  /// [communicationIsolate.Communicationisolate.dispose()]
  static void requestIsolateShutdown(){
    communicationIsolate.Communicationisolate.sendToIsolate(communicationIsolate.IsolateMessage(IsolateOperationType.
    ShutDown));
  }

  /// makes sure to update the [communicationIsolate.Communicationisolate] as well.
  static void _removeUserFromMap(String address){
    usersList?.remove(address);
    communicationIsolate.Communicationisolate.sendToIsolate(communicationIsolate.IsolateMessage(IsolateOperationType.
    KickClient, content: {IsolateDataType.InternetAddressString : address}));
  }

  static void closeManager(){
    communicationIsolate.Communicationisolate.sendToIsolate(communicationIsolate.IsolateMessage(IsolateOperationType.CloseManager));
  }
  
  static void closeClient(){
    communicationIsolate.Communicationisolate.sendToIsolate(communicationIsolate.IsolateMessage(IsolateOperationType.CloseClient));
  }
  
  static void closeSongListener(){
    communicationIsolate.Communicationisolate.sendToIsolate(communicationIsolate.IsolateMessage(IsolateOperationType.CloseP2pListener));
  }

  /// This will request time sync from the server, and the proccess will automatically happen.
  static void requestTimeReSync(){
    AppConfig.timeSyncResults.clear();
    Communication.sendMessage(TimeSync(AppConfig.applicationTime.millisecondsSinceEpoch, 0), TcpSocketType.ManagerListenerSocket);
  }

  static void alertClientSet(){
    joinRoomBackend.JoinRoom.currentInstance?.clientIsSet();
  }

  /// Kicks client from the server, sends him kick msg and removes him from the map(both isolates.)
  static void kickClient(String address){
    sendMessage(KickClient(), TcpSocketType.ClientSocket, sockAddr: address);
    _removeUserFromMap(address);

    ManagerRoomLandingPage.currentInstance?.currentClientsAmount--;
    ManagerRoomLandingPage.currentInstance?.alertUsersListUpdated();
    ManagerRoomLandingPage.currentInstance?.sendStatusToAllClients(exclude: [address, ConstantValues.localLoopback.address]);

    //TODO: handle in manager room page.
    //     
    // 
    // here
    //     
  }

  static void handleIncomingPackedMsg(Uint8List packed, 
  TcpSocketType receivingNetworkType, {String socketAddr}){
    if(packed == null)
      return;
    var message = Message.unpack(packed);
    if(message.errCode == -1 || message.errCode == null){
      try
      {
        switch(MessageNum.values[message.messageCode])
        {
          case MessageNum.Multicast:
            break;
          case MessageNum.GetRoomData:
            if(receivingNetworkType == TcpSocketType.ManagerListenerSocket)
            {
              var instance = ManagerRoomLandingPage.currentInstance;
              var toReply = GetRoomData(instance?.maxClients, instance?.currentClientsAmount, instance?.roomName);
              sendMessage(toReply, TcpSocketType.ClientSocket, sockAddr: socketAddr);
            }
            else if(receivingNetworkType == TcpSocketType.ClientSocket)
            {
              var roomData = GetRoomData.unpack(packed);
              joinRoomBackend.JoinRoom.currentInstance?.roomData = roomData;
              joinRoomBackend.JoinRoom.currentInstance?.alertListeners();
            }
            break;
          case MessageNum.JoinRoom:
            if(receivingNetworkType == TcpSocketType.ManagerListenerSocket)
            {
              var unpacked = JoinRoom.unpack(packed);
              bool update = false;
              var instance = ManagerRoomLandingPage.currentInstance;
              var toSend = Message(unpacked.messageCode);
              if(instance != null && instance.isAccepting)
              {
                if(instance.currentClientsAmount < instance.maxClients){
                  if(instance.password != unpacked.password)
                    toSend.errCode = ErrorCodes.WrongRoomPassword.index;
                  else{
                    if(usersList == null) usersList = new Map();
                    if(connectedUsers.values.where((s){return s == instance.userName;}).length > 0)
                      toSend.errCode = ErrorCodes.PlayerNameAlreadyTaken.index;
                    else
                    {
                      usersList[socketAddr] = unpacked.userName;
                      instance.currentClientsAmount++;
                      instance.alertUsersListUpdated();
                      update = true;
                    }
                  }
                }
                else{
                  toSend.errCode = ErrorCodes.CantJoinRoomMaxClientsReached.index;
                }
              }
              else
                toSend.errCode = ErrorCodes.PartyAlreadyStarted.index;
              sendMessage(toSend, TcpSocketType.ClientSocket, sockAddr: socketAddr);
              if(update) instance.sendStatusToAllClients(exclude:[socketAddr], sendWithDelayToExcluded: true);
            }
            else if(receivingNetworkType == TcpSocketType.ClientSocket)
            {
              if(joinRoomBackend.JoinRoom.currentInstance == null)
                return;
              joinRoomBackend.JoinRoom.currentInstance.isReady = true;
              joinRoomBackend.JoinRoom.currentInstance?.alertListeners();
            }
            break;
          case MessageNum.TimeSync:
            /// TimeSync handler, this operation happens automatically once a user calls the [Communication.requestTimeReSync] method
            var timeSync = TimeSync.unpack(packed);
            if(receivingNetworkType == TcpSocketType.ManagerListenerSocket){
              sendMessage(TimeSync(timeSync.clientTime, AppConfig.applicationTime.millisecondsSinceEpoch), TcpSocketType.ClientSocket, 
                sockAddr: socketAddr);
            }
            else if(receivingNetworkType == TcpSocketType.ClientSocket){
              var result = new TimeSyncResult(timeSync, (AppConfig.applicationTime.millisecondsSinceEpoch - 
                timeSync.clientTime));
              if(AppConfig.timeSyncResults.length == 0)
                AppConfig.setAppTime(result);
              AppConfig.timeSyncResults.addInOrder(result);
              if(AppConfig.timeSyncResults.length < ConstantValues.amountOfTimesForSync)
                sendMessage(TimeSync(AppConfig.applicationTime.millisecondsSinceEpoch, 0), TcpSocketType.ManagerListenerSocket);
              else{
                var bestIndex = (AppConfig.timeSyncResults.length % 2 == 0 ? 
                  (AppConfig.timeSyncResults.length / 2).round() : ((AppConfig.timeSyncResults.length + 1) / 2)).round();
                var finalList = AppConfig.timeSyncResults.sublist(bestIndex > 0 ? bestIndex - 1 : 0, 
                  bestIndex + 1 > AppConfig.timeSyncResults.length ? bestIndex : bestIndex + 1);
                var finalResultClientTime = 0;
                var finalResultServerTime = 0;
                var finalResultLatency = 0;
                for(var curr in finalList){
                  finalResultClientTime += curr.timeSync.clientTime;
                  finalResultServerTime += curr.timeSync.serverTime;
                  finalResultLatency += curr.latency;
                }
                finalResultClientTime = (finalResultClientTime / finalList.length).round();
                finalResultServerTime = (finalResultServerTime / finalList.length).round();
                finalResultLatency = (finalResultLatency / finalList.length).round();
                // TODO: Make the sync better.
                AppConfig.setAppTime(TimeSyncResult(TimeSync(finalResultClientTime, finalResultServerTime), finalResultLatency));
              }
            }
            break;
          case MessageNum.RoomStart:
            //var roomStart = RoomStart.unpack(packed);
            if(receivingNetworkType == TcpSocketType.ClientSocket){
              ClientRoomLandingPage.instance?.roomStart = true;
              ClientRoomLandingPage.instance?.alertRoomStatusUpdate();
            }
            else if(receivingNetworkType == TcpSocketType.ManagerListenerSocket){
              ManagerRoomLandingPage.currentInstance?.confirmedClientsIps?.add(socketAddr);
            }
            break;
          case MessageNum.KickClient:
            //var kickClient = KickClient.unpack(packed);
            if(receivingNetworkType == TcpSocketType.ClientSocket){
              ClientRoomLandingPage.instance?.kicked = true;
              ClientRoomLandingPage.instance?.alertRoomStatusUpdate();
              //TODO: handle in client room page.
            }
            else if(receivingNetworkType == TcpSocketType.ManagerListenerSocket){
              Communication.kickClient(socketAddr);
            }
            break;
          case MessageNum.RoomStatus:
            var roomStatus = RoomStatus.unpack(packed);
            if(receivingNetworkType == TcpSocketType.ClientSocket){
              ClientRoomLandingPage.instance?.roomStatus = roomStatus;
              ClientRoomLandingPage.instance?.alertRoomStatusUpdate();
              //TODO: handle in client room page.
            }
            break;
          case MessageNum.SongData:
            var songData = SongData.unpack(packed);
            break;
          case MessageNum.SongDataConfirm:
            var songDataConfirm = SongDataConfirm.unpack(packed);
            break;
          case MessageNum.PlayData:
            var playData = PlayData.unpack(packed);
            break;
          case MessageNum.QuitRoom:
            var quitRoom = QuitRoom.unpack(packed);
            break;
          default:
            print(ConstantValues.messageTypeUndefined);
            break;
        }
      }
      catch(e){print("General communication msg handler: " + e.toString() + " msg: " + message.toString());}
    }
    else{
      var err = ErrorCodes.values[message.errCode];
      switch(err){
        case ErrorCodes.GeneralError:
          print("General Error: " + err.toString());
          break;
        case ErrorCodes.CantJoinRoomMaxClientsReached:
          joinRoomBackend.JoinRoom.currentInstance?.maxClientsReached = true;
          joinRoomBackend.JoinRoom.currentInstance?.alertListeners();
          break;
        case ErrorCodes.PartyAlreadyStarted:
          joinRoomBackend.JoinRoom.currentInstance?.alreadyStarted = true;
          joinRoomBackend.JoinRoom.currentInstance?.alertListeners();
          break;
        case ErrorCodes.NetworkError:
          // TODO: Handle this case.
          break;
        case ErrorCodes.WrongRoomPassword:
          joinRoomBackend.JoinRoom.currentInstance?.wrongPass = true;
          joinRoomBackend.JoinRoom.currentInstance?.alert= true;
          joinRoomBackend.JoinRoom.currentInstance?.alertListeners();
          break;
        case ErrorCodes.NoError:
          // TODO: Handle this case.
          break;
        case ErrorCodes.PlayerNameAlreadyTaken:
          joinRoomBackend.JoinRoom.currentInstance?.nameTaken = true;
          joinRoomBackend.JoinRoom.currentInstance?.alertListeners();
          break;
      }
    }
  }
}