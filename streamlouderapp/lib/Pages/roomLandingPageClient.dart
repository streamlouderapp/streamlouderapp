import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:streamlouderapp/base/appConfig.dart';
import 'package:streamlouderapp/base/definitions.dart';
import 'package:streamlouderapp/base/general.dart';
import 'package:streamlouderapp/models/clientRoomLandingPage.dart';
import 'package:streamlouderapp/models/dataStructs.dart';
import 'package:streamlouderapp/widgets/custom_widgets.dart' as customWidgets;

class RoomLandingPageClient extends StatelessWidget{
  RoomLandingPageClient(this._data, this._yourName);

  final GetRoomData _data;
  final String _yourName;

  static bool _shouldPop = false;
  @override
  Widget build(BuildContext context) {
    return Directionality(textDirection: Helper.textDirection(),
    child: ChangeNotifierProvider<ClientRoomLandingPage>(
      child: new WillPopScope(
        onWillPop: () async => _shouldPop,
        child: _CustomContent(_data.roomName)),
      create: (context)=> ClientRoomLandingPage(_data, _yourName)));
  }
}

class _CustomContent extends StatelessWidget{
  final String _roomName;
  _CustomContent(this._roomName);
  Widget build(BuildContext context){
    ClientRoomLandingPage logic = Provider.of(context);
    return Scaffold(appBar: customWidgets.CustomAppBar(title: _roomName, 
        leading: IconButton(icon: Icon(Icons.close), 
        onPressed: (){
          Helper.pushDialog(context, AppConfig.localizedStrings.notice(), 
            AppConfig.localizedStrings.wouldYouLikeToLeave(), actions: <ButtonsAndAction>
            [ButtonsAndAction(context, AppConfig.localizedStrings.yes(), 
            func: (){
              logic.quitRoom();
              while(Navigator.canPop(context)){
                Navigator.pop(context);
              }
            }),
            ButtonsAndAction(context, AppConfig.localizedStrings.no()),
            ]);
        })),
        backgroundColor: ConstantValues.seconderyBackgroundColor,
        body: Center(child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
        Container(width: AppConfig.screenWidth * ConstantValues.roomsListWidthPercentageOfScreen,
        height: AppConfig.screenHeight * ConstantValues.roomsListHeightPercentageOfScreen,
        decoration: BoxDecoration(
        color: ConstantValues.mainBackgroundColor, borderRadius: 
        BorderRadius.all(Radius.circular(ConstantValues.roundRadius))),
        child: Consumer<ClientRoomLandingPage>(
        builder: (context, myModel, child) {
          if(myModel.roomStart){
            //TODO: link to client room page
          }
          else if(myModel.kicked){
            Future.delayed(Duration.zero, (){Navigator.of(context).pop();});
          }
          return Container(padding: EdgeInsets.all(AppConfig.screenWidth * ConstantValues.applicationWidthMarginPercentage),
          child:
            myModel.roomStatus == null ? customWidgets.CustomCircularProgressBar() :
            new Column(crossAxisAlignment: CrossAxisAlignment.center, mainAxisAlignment: MainAxisAlignment.start,
            children:<Widget>[ 
              Text(AppConfig.localizedStrings.connectedUsers() + " " +
                myModel.roomStatus.clients.length.toString() + "/" + myModel.roomData.amountOfMaxClients.toString(),
                textAlign: TextAlign.center,
                style:TextStyle(color: ConstantValues.seconderyBackgroundColor, fontSize: ConstantValues.titleFontSize)),
              Container(height: AppConfig.screenHeight * ConstantValues.usersListHeightPercentageOfScreen,
              child: new ListView.builder
              (
                itemCount: myModel.roomStatus.clients.length,
                itemBuilder: (BuildContext ctxt, int index) => FlatButton(splashColor: ConstantValues.seconderyBackgroundColor,
                onPressed: (){},
                child: Text(myModel.roomStatus.clients[
                myModel.roomStatus.clients.keys.elementAt(index)], textAlign: TextAlign.center,  
                style: TextStyle(color: myModel.roomStatus.clients.keys.elementAt(index) == ConstantValues.localLoopback.address ?
                ConstantValues.managerNameColor : ConstantValues.seconderyBackgroundColor, fontSize: ConstantValues.defualtFontSize),),)
              ))
            ]));
        }),
        )])));
  }
}