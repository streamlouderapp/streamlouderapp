import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:streamlouderapp/base/appConfig.dart';
import 'package:streamlouderapp/base/definitions.dart';
import 'package:streamlouderapp/base/general.dart';
import 'package:streamlouderapp/models/createRoom.dart';
import 'package:streamlouderapp/pages/roomLandingPageManager.dart';
import 'package:streamlouderapp/widgets/custom_widgets.dart' as customWidgets;

class RoomCreation extends StatelessWidget{
  static String roomName;
  static String password;
  static int clientAmount;
  static String yourName;
  static bool shouldPop = true;

  @override
  Widget build(BuildContext context) {
    return Directionality(textDirection: Helper.textDirection(),
      child: ChangeNotifierProvider<CreateRoom>(
      child: new WillPopScope(
        onWillPop: () async => shouldPop,
        child: Scaffold(appBar: customWidgets.CustomAppBar(title: AppConfig.localizedStrings.roomCreation(), 
        leading: IconButton(icon: Icon(Icons.arrow_back), 
        onPressed: (){Navigator.pop(context);})),
        backgroundColor: ConstantValues.seconderyBackgroundColor,
        body: Center(child: ListView(padding: EdgeInsets.all(AppConfig.screenWidth * ConstantValues.applicationWidthPaddingPercentage),
        children: <Widget>[
        Container(width: AppConfig.screenWidth * ConstantValues.roomsListWidthPercentageOfScreen,
        height: AppConfig.screenHeight * ConstantValues.roomsListHeightPercentageOfScreen,
        padding: EdgeInsets.all(AppConfig.screenWidth * ConstantValues.applicationWidthMarginPercentage),
        decoration: BoxDecoration(
        color: ConstantValues.mainBackgroundColor, borderRadius: 
        BorderRadius.all(Radius.circular(ConstantValues.roundRadius))),
        child: Consumer<CreateRoom>(
        builder: (ctxt, myModel, child) {
          if(myModel.isReady){
            shouldPop = false;
            Future.delayed(Duration.zero, (){shouldPop = true;
            while(Navigator.canPop(context)){Navigator.pop(context);}
            Navigator.push(context, MaterialPageRoute(builder: (context) => RoomLandingPageManager(clientAmount, password, roomName, yourName)));});
            return customWidgets.CustomCircularProgressBar();
          }
          else
          {
            if(myModel.alert)
            {
              String textForPopup;
              if(!myModel.stringsValid)
                textForPopup = AppConfig.localizedStrings.stringAreInvalid();
              else if(!myModel.clientsAmountValid)
                textForPopup = AppConfig.localizedStrings.clientAmountInvalid();
              Future.delayed(Duration.zero, (){Helper.pushDialog(context, AppConfig.localizedStrings.notice(), textForPopup, 
              actions: [ButtonsAndAction(context, AppConfig.localizedStrings.understood())]);});
            }
            var style = TextStyle(color: ConstantValues.seconderyBackgroundColor);
            var borderLight = OutlineInputBorder(
                borderSide: const BorderSide(color: ConstantValues.seconderyBackgroundColorLight, width: ConstantValues.roundBorderWidth),
                borderRadius: BorderRadius.circular(ConstantValues.roundRadius)
              );
            var borderDark = OutlineInputBorder(
                borderSide: const BorderSide(color: ConstantValues.seconderyBackgroundColorDark, width: ConstantValues.roundBorderWidth),
                borderRadius: BorderRadius.circular(ConstantValues.roundRadius)
              );
            var border = (AppConfig.brightness == Brightness.light) ? borderLight : borderDark;
            var margin = EdgeInsets.all(AppConfig.screenWidth * ConstantValues.applicationWidthMarginPercentage);
            return Center(child: ListView(children: <Widget>[
              Container(child:
              TextFormField(onChanged: (String changed){roomName = changed;},
              maxLength: ConstantValues.maxStringLength,
              decoration: InputDecoration(
              counterStyle: TextStyle(color: ConstantValues.seconderyBackgroundColor),
              hintStyle: style,
              hintText: AppConfig.localizedStrings.roomName(),
              fillColor: ConstantValues.seconderyBackgroundColor,
              hoverColor: ConstantValues.seconderyBackgroundColor,
              focusedBorder: border,
              enabledBorder: border), style: style,), margin: margin),
              Container(child:
              TextFormField(onChanged: (String changed){password = changed;},
              obscureText: true,
              maxLength: ConstantValues.maxStringLength,
              decoration: InputDecoration(
              counterStyle: TextStyle(color: ConstantValues.seconderyBackgroundColor),
              hintStyle: style,
              hintText: AppConfig.localizedStrings.roomPassword(),
              fillColor: ConstantValues.seconderyBackgroundColor,
              hoverColor: ConstantValues.seconderyBackgroundColor,
              focusedBorder: border,
              enabledBorder: border), style: style,), margin: margin),
              Container(child:
              TextFormField(onChanged: (String changed){var amount = int.tryParse(changed); amount == null ? 
              clientAmount = ConstantValues.minimumMaxClients : clientAmount = amount;},
              keyboardType: TextInputType.number,
              maxLength: ConstantValues.maxIntStringLength,
              decoration: InputDecoration(
              counterStyle: TextStyle(color: ConstantValues.seconderyBackgroundColor),
              hintStyle: style,
              hintText: AppConfig.localizedStrings.maxClientAmount(),
              fillColor: ConstantValues.seconderyBackgroundColor,
              hoverColor: ConstantValues.seconderyBackgroundColor,
              focusedBorder: border,
              enabledBorder: border), style: style,), margin: margin),
              Container(child:
              TextFormField(onChanged: (String changed){yourName = changed;},
              maxLength: ConstantValues.maxStringLength,
              decoration: InputDecoration(
              counterStyle: TextStyle(color: ConstantValues.seconderyBackgroundColor),
              hintStyle: style,
              hintText: AppConfig.localizedStrings.yourName(),
              fillColor: ConstantValues.seconderyBackgroundColor,
              hoverColor: ConstantValues.seconderyBackgroundColor,
              focusedBorder: border,
              enabledBorder: border), style: style,), margin: margin),
            ]));
          }
        }),
        ),
        Container(margin: EdgeInsets.all(AppConfig.screenWidth * ConstantValues.applicationWidthMarginPercentage),
        child: _CreateButton())])))),
      create: (context)=> CreateRoom()));
  }
  
}

class _CreateButton extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return customWidgets.RoundFlatButton(btnBackgroundColor: ConstantValues.mainBackgroundColor,
      onPressed: (){Provider.of<CreateRoom>(context, listen: false).createRoom(RoomCreation.roomName, RoomCreation.password, RoomCreation.clientAmount,
      RoomCreation.yourName);},
      fontSize: ConstantValues.defualtFontSize,
      splashColor: ConstantValues.seconderyBackgroundColor,
      textColor: ConstantValues.seconderyBackgroundColor,
      text: AppConfig.localizedStrings.create());
  }
}