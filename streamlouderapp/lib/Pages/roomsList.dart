import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:streamlouderapp/Pages/roomCreation.dart';
import 'package:streamlouderapp/base/appConfig.dart';
import 'package:streamlouderapp/base/definitions.dart';
import 'package:streamlouderapp/base/general.dart';
import 'package:streamlouderapp/models/languageHandler.dart';
import 'package:streamlouderapp/models/roomListScreenLogic.dart';
import 'package:flutter/rendering.dart';
import 'package:streamlouderapp/widgets/custom_widgets.dart' as customWidgets;

class RoomsListPage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    debugPaintSizeEnabled = false;
    AppConfig.init(context, AppConfig.language != null ? AppConfig.language : Language.EN);
    var btnMargin = EdgeInsets.fromLTRB(AppConfig.screenWidth * ConstantValues.applicationWidthMarginPercentage, 
        AppConfig.screenWidth * ConstantValues.applicationWidthMarginPercentage, 
        AppConfig.screenWidth * ConstantValues.applicationWidthMarginPercentage, 0);
    return Directionality(textDirection: Helper.textDirection(),
    child: ChangeNotifierProvider<RoomListScreenLogic>(
      child: Scaffold(appBar: customWidgets.CustomAppBar(title: AppConfig.localizedStrings.selectARoom(), 
      leading: IconButton(icon: Icon(Icons.add), 
      onPressed: (){Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => RoomCreation()),
      );})),
      backgroundColor: ConstantValues.seconderyBackgroundColor,
      body: Center(child: ListView(padding: EdgeInsets.all(AppConfig.screenWidth * ConstantValues.applicationWidthPaddingPercentage),
      children: <Widget>[
      Container(width: AppConfig.screenWidth * ConstantValues.roomsListWidthPercentageOfScreen,
      height: AppConfig.screenHeight * ConstantValues.roomsListHeightPercentageOfScreen,
      padding: EdgeInsets.all(AppConfig.screenWidth * ConstantValues.applicationWidthMarginPercentage),
      decoration: BoxDecoration(
      color: ConstantValues.mainBackgroundColor, borderRadius: 
      BorderRadius.all(Radius.circular(ConstantValues.roundRadius))),
      child: Consumer<RoomListScreenLogic>( //                  <--- Consumer
      builder: (context, myModel, child) {
        return (myModel.roomsList == null || myModel.roomsList.length == 0) ? 
        customWidgets.CustomCircularProgressBar() : 
        new ListView.builder
        (
          itemCount: myModel.roomsList.length,
          itemBuilder: (BuildContext ctxt, int index) => FlatButton(splashColor: ConstantValues.seconderyBackgroundColor,
          onPressed: (){Helper.pushPopup(ctxt, customWidgets.JoinRoomPopupContent(myModel.roomsList.keys.elementAt(index)));},
          child: Text(myModel.roomsList[
          myModel.roomsList.keys.elementAt(index)], textAlign: TextAlign.center,  
          style: TextStyle(color: ConstantValues.seconderyBackgroundColor, fontSize: ConstantValues.defualtFontSize),),)
        );
      }),
      ),
      Container(margin: btnMargin,
      child: _RefreshBtn()),
      Container(margin: btnMargin,
      child: _ToggleThemeBtn()),
      Container(margin: btnMargin,
      child: _LangBtn())]))),
      create: (context)=> RoomListScreenLogic()));
  }
}

class _RefreshBtn extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return customWidgets.RoundFlatButton(btnBackgroundColor: ConstantValues.mainBackgroundColor,
      onPressed: (){
        Provider.of<RoomListScreenLogic>(context, listen: false).refreshRoomList();
        },
      fontSize: ConstantValues.defualtFontSize,
      splashColor: ConstantValues.seconderyBackgroundColor,
      textColor: ConstantValues.seconderyBackgroundColor,
      text: AppConfig.localizedStrings.refresh());
  }
}

class _ToggleThemeBtn extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return customWidgets.RoundFlatButton(btnBackgroundColor: ConstantValues.mainBackgroundColor,
      onPressed: (){
        Helper.changeBrightness(context);
        },
      fontSize: ConstantValues.defualtFontSize,
      splashColor: ConstantValues.seconderyBackgroundColor,
      textColor: ConstantValues.seconderyBackgroundColor,
      text: AppConfig.localizedStrings.toggleBrightness());
  }
}

class _LangBtn extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return ChangeNotifierProvider<LanguageHandler>(create: (context)=> LanguageHandler(),
    child: Consumer<LanguageHandler>(builder: (context, model, child){
      return Theme(data: ThemeData(canvasColor: ConstantValues.seconderyBackgroundColor),child:
        Align(alignment: Alignment.center,child:
        DropdownButton<String>(
          value: model.language.toString().substring(ConstantValues.indexForLanguageSubstring),
          icon: Icon(Icons.keyboard_arrow_down, color: ConstantValues.mainBackgroundColor,),
          style: TextStyle(
            color: ConstantValues.mainBackgroundColor,
          ),
          underline: Container(
            height: ConstantValues.underlineHeight,
            color: ConstantValues.mainBackgroundColor,
          ),
          onChanged: (String newValue) {
            model.setLang(newValue);
          },
          items: Language.values
            .map<DropdownMenuItem<String>>((Language value) {
              return DropdownMenuItem<String>(
                value: value.toString().substring(ConstantValues.indexForLanguageSubstring),
                child: Text(value.toString().substring(ConstantValues.indexForLanguageSubstring)),
              );
            })
            .toList(),
        )));
    }));
  }
}