import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:streamlouderapp/base/appConfig.dart';
import 'package:streamlouderapp/base/definitions.dart';
import 'package:streamlouderapp/base/general.dart';
import 'package:streamlouderapp/models/managerRoomLandingPage.dart';
import 'package:streamlouderapp/widgets/custom_widgets.dart' as customWidgets;

class RoomLandingPageManager extends StatelessWidget{
  RoomLandingPageManager(this._clientAmount, this._password, this._roomName, this._yourName);

  final String _roomName;
  final String _password;
  final int _clientAmount;
  final String _yourName;

  static bool _shouldPop = false;
  @override
  Widget build(BuildContext context) {
    return Directionality(textDirection: Helper.textDirection(),
    child: ChangeNotifierProvider<ManagerRoomLandingPage>(
      child: new WillPopScope(
        onWillPop: () async => _shouldPop,
        child: _CustomContent(_roomName)),
      create: (context)=> ManagerRoomLandingPage(_roomName, _password, _yourName, _clientAmount)));
  }
}

class _StartPartyButton extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return customWidgets.RoundFlatButton(btnBackgroundColor: ConstantValues.mainBackgroundColor,
      onPressed: (){Provider.of<ManagerRoomLandingPage>(context, listen: false).startParty();},
      fontSize: ConstantValues.defualtFontSize,
      splashColor: ConstantValues.seconderyBackgroundColor,
      textColor: ConstantValues.seconderyBackgroundColor,
      text: AppConfig.localizedStrings.startParty());
  }
}

class _CustomContent extends StatelessWidget{
  final String _roomName;
  _CustomContent(this._roomName);
  Widget build(BuildContext context){
    ManagerRoomLandingPage logic = Provider.of(context);
    return Scaffold(appBar: customWidgets.CustomAppBar(title: _roomName, 
        leading: IconButton(icon: Icon(Icons.close), 
        onPressed: (){
          logic.cancelRoom();
          Navigator.pop(context);})),
        backgroundColor: ConstantValues.seconderyBackgroundColor,
        body: Center(child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
        Container(width: AppConfig.screenWidth * ConstantValues.roomsListWidthPercentageOfScreen,
        height: AppConfig.screenHeight * ConstantValues.roomsListHeightPercentageOfScreen,
        decoration: BoxDecoration(
        color: ConstantValues.mainBackgroundColor, borderRadius: 
        BorderRadius.all(Radius.circular(ConstantValues.roundRadius))),
        child: Consumer<ManagerRoomLandingPage>(
        builder: (context, myModel, child) {
          if(myModel.isReady){
            /*TODO: link to manager room page */
          }
          return Container(padding: EdgeInsets.all(AppConfig.screenWidth * ConstantValues.applicationWidthMarginPercentage),
          child:
          (!myModel.isAccepting) ? 
          customWidgets.CustomCircularProgressBar() :
          new Column(crossAxisAlignment: CrossAxisAlignment.center, mainAxisAlignment: MainAxisAlignment.start,
          children:<Widget>[ 
            Text(AppConfig.localizedStrings.connectedUsers() + " " +
              myModel.currentClientsAmount.toString() + "/" + myModel.maxClients.toString(),
              textAlign: TextAlign.center,
              style:TextStyle(color: ConstantValues.seconderyBackgroundColor, fontSize: ConstantValues.titleFontSize)),
            (myModel.connectedUsers == null || myModel.connectedUsers.length == 0) ?
            customWidgets.CustomCircularProgressBar() :
            Container(height: AppConfig.screenHeight * ConstantValues.usersListHeightPercentageOfScreen,
            child: new ListView.builder
            (
              itemCount: myModel.connectedUsers.length,
              itemBuilder: (BuildContext ctxt, int index) => FlatButton(splashColor: ConstantValues.seconderyBackgroundColor,
              onPressed: (){
              Helper.pushPopup(ctxt, customWidgets.KickUserPopupContent(myModel.connectedUsers[
              myModel.connectedUsers.keys.elementAt(index)], myModel.connectedUsers.keys.elementAt(index)));},
              child: Text(myModel.connectedUsers[
              myModel.connectedUsers.keys.elementAt(index)], textAlign: TextAlign.center,  
              style: TextStyle(color: myModel.connectedUsers.keys.elementAt(index) == ConstantValues.localLoopback.address ?
              ConstantValues.managerNameColor : ConstantValues.seconderyBackgroundColor, fontSize: ConstantValues.defualtFontSize),),)
            ))
          ]));
        }),
        ),
        Container(margin: EdgeInsets.all(AppConfig.screenWidth * ConstantValues.applicationWidthMarginPercentage),
        child: _StartPartyButton())])));
  }
}