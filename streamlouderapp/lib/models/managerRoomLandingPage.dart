import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:streamlouderapp/base/definitions.dart';
import 'package:streamlouderapp/base/general.dart';
import 'package:streamlouderapp/models/dataStructs.dart';
import 'package:streamlouderapp/models/roomListScreenLogic.dart';

class ManagerRoomLandingPage with ChangeNotifier{
  String roomName;
  String password;
  String userName;
  int maxClients;
  bool isAccepting;
  bool isReady;
  int currentClientsAmount;
  List<String> confirmedClientsIps;
  Map<String, String> get connectedUsers{
    var ret = {ConstantValues.localLoopback.address : this.userName};
    if(Communication.connectedUsers != null) ret.addAll(Communication.connectedUsers);
    /*ret.addAll({
      "192.168.1.3" : "User on local network",
      "192.168.1.4" : "איתן2",
      "192.168.1.5" : "c",
      "192.168.1.6" : "d",
      "192.168.1.7" : "e",
      "192.168.1.8" : "f",
      "192.168.1.9" : "g",
      "192.168.1.10" : "hhhhhhhhhhhhhhhhhhhhhhhhhhhh",
    });*/
    return ret;
  }

  void alertUsersListUpdated(){
    notifyListeners();
  }

  static ManagerRoomLandingPage currentInstance;

  @override
  void dispose(){
    super.dispose();
    currentInstance = null;
  }

  ManagerRoomLandingPage(this.roomName, this.password, this.userName, this.maxClients){
    currentInstance = this;
    isAccepting = true;
    isReady = false;
    this.currentClientsAmount = 1; // only the manager is joined at this point
    Communication.sendBroadcast(roomName: roomName); // we want to send the broadcast only once we are "in"
                                                    // the room. (The room landing page is already considered the room)
  }

  void cancelRoom(){
    Communication.stopBroadcast();
    for(var curr in connectedUsers.keys){
      Communication.kickClient(curr);
    } //kicking everyone out.
    Communication.closeManager();
    RoomListScreenLogic.instance?.refreshRoomList();
  }

  void sendStatusToAllClients({List<String> exclude, bool sendWithDelayToExcluded = false}){
    for(var curr in connectedUsers.keys)
    {
      if(exclude == null || exclude.where((s) => s == curr).length == 0)
        Communication.sendMessage(RoomStatus(Map<String,String>(), connectedUsers, null), TcpSocketType.ClientSocket, sockAddr: curr);
    }
    if(sendWithDelayToExcluded && exclude != null && exclude.length > 0){
      Future.delayed(ConstantValues.durationSendStatusDelay, (){
        for(var curr in connectedUsers.keys){
          if(exclude.where((t)=> t == curr).length > 0)// if the user is inside the excluded list.
            Communication.sendMessage(RoomStatus(Map<String,String>(), connectedUsers, null), TcpSocketType.ClientSocket, sockAddr: curr);
        }
      });
    }
  }

  void startParty(){
    Communication.stopBroadcast();
    for(var curr in connectedUsers.keys)
      Communication.sendMessage(RoomStart(), TcpSocketType.ClientSocket, sockAddr: curr);
    isAccepting = false;
    confirmedClientsIps = new List();
    Future.delayed(ConstantValues.durationTillRoomFinalStart, (){
      for(var ip in connectedUsers.keys){
        if(confirmedClientsIps.where((t)=> t == ip).length == 0){//means the user isn't confirmed.
          Communication.kickClient(ip);
        }
      }
      isReady = true;
      notifyListeners();
    });
  }


}