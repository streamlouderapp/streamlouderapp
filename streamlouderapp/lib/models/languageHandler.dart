import 'package:flutter/cupertino.dart';
import 'package:streamlouderapp/base/appConfig.dart';
import 'package:streamlouderapp/base/definitions.dart';
import 'package:streamlouderapp/main.dart';

class LanguageHandler with ChangeNotifier{
  Language language;
  LanguageHandler(){
    language = AppConfig.language;
  }
  void setLang(String newS){
    var lang = Language.values.where((lang)=> lang.toString().substring(ConstantValues.indexForLanguageSubstring) == newS).first;
    language = lang;
    AppConfig.setLanguage(lang);
    MyApp.restart();
  }
}