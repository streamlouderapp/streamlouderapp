
import 'dart:typed_data';

import 'package:msgpack_dart/msgpack_dart.dart';
//import 'package:streamlouderapp/base/definitions.dart';

enum MessageNum{
  Multicast,
  GetRoomData,
  JoinRoom,
  TimeSync,
  RoomStart,
  KickClient,
  RoomStatus,
  SongData,
  SongDataConfirm,
  PlayData,
  QuitRoom
}

enum MessageKeys{
  messageCode,
  roomName,
  amountOfMaxClients,
  currentClientsAmount,
  userName,
  password,
  clientTime,
  serverTime,
  songQueue,
  currentSongHash,
  clients,
  sendTo,
  songHash,
  receivedAll,
  wantedSongs,
  startAt,
  errCode,
  songData
}

class Message {
  Message(this.messageCode, {this.errCode});
  int messageCode;
  int errCode = -1;
  Uint8List pack() => serialize({MessageKeys.messageCode.index : messageCode, MessageKeys.errCode.index: errCode});
  factory Message.unpack(Uint8List packed){
    var d = deserialize(packed);
    return Message(d[MessageKeys.messageCode.index], errCode: d[MessageKeys.errCode.index]);
  }
  @override
  String toString() {
    return "MsgCode: " + messageCode.toString() + " ErrCode: " + errCode.toString();
  }
}

class GetRoomData extends Message{
  GetRoomData(this.amountOfMaxClients, this.currentClientsAmount, this.roomName) : 
  super(MessageNum.GetRoomData.index);

  String roomName;
  int amountOfMaxClients;
  int currentClientsAmount;

  @override
  Uint8List pack() => serialize({MessageKeys.messageCode.index : super.messageCode, 
  MessageKeys.roomName.index : roomName, MessageKeys.amountOfMaxClients.index : amountOfMaxClients, 
  MessageKeys.currentClientsAmount.index : currentClientsAmount});

  @override
  factory GetRoomData.unpack(Uint8List packed){
    var d = deserialize(packed);
    return GetRoomData(d[MessageKeys.amountOfMaxClients.index], d[MessageKeys.currentClientsAmount.index], d[MessageKeys.roomName.index]);
  }
}

class JoinRoom extends Message {
  JoinRoom(this.userName, this.password) : super(MessageNum.JoinRoom.index);

  String userName;
  String password;
  
  @override
  Uint8List pack() => serialize({MessageKeys.messageCode.index : super.messageCode,
  MessageKeys.userName.index : userName, MessageKeys.password.index : password});

  @override
  factory JoinRoom.unpack(Uint8List packed){
    var d = deserialize(packed);
    return JoinRoom(d[MessageKeys.userName.index], d[MessageKeys.password.index]);
  }
}

/// Time Sync message, 
class TimeSync extends Message {
  TimeSync(this.clientTime, this.serverTime) : super(MessageNum.TimeSync.index);

  int clientTime;
  int serverTime;
  DateTime get clientDateTime {
    return DateTime.fromMillisecondsSinceEpoch(clientTime, isUtc: true);
  }
  
  DateTime get serverDateTime {
    return DateTime.fromMillisecondsSinceEpoch(serverTime, isUtc: true);
  }
  
  @override
  Uint8List pack() => serialize({MessageKeys.messageCode.index : super.messageCode,
  MessageKeys.clientTime.index : clientTime, MessageKeys.serverTime.index : serverTime});

  @override
  factory TimeSync.unpack(Uint8List packed){
    var d = deserialize(packed);
    return TimeSync(d[MessageKeys.clientTime.index], d[MessageKeys.serverTime.index]);
  }
}

class TimeSyncResult{
  TimeSyncResult(this.timeSync, this.latency);
  TimeSync timeSync;
  int latency;
}

class RoomStart extends Message{
  RoomStart() : super(MessageNum.RoomStart.index);
  ///reserved for duture development
  @override
  Uint8List pack() => super.pack();

  @override
  factory RoomStart.unpack(Uint8List packed){
    return Message.unpack(packed);
  }
}

class KickClient extends Message{
  ///reserved for future development
  KickClient() : super(MessageNum.KickClient.index);
  @override
  Uint8List pack() => super.pack();

  @override
  factory KickClient.unpack(Uint8List packed){
    return Message.unpack(packed);
  }
}

class RoomStatus extends Message {
  RoomStatus(dynamic queue, dynamic clients, this.currentSongHash) : super(MessageNum.RoomStatus.index){
    this.songQueue = Map<String,String>.from(queue);
    this.clients = Map<String,String>.from(clients);
  }
  Map<String, String> songQueue;// ip addresses and songHash
  String currentSongHash;
  Map<String, String> clients;// ip addresses and usernames

  @override
  Uint8List pack() => serialize({MessageKeys.messageCode.index : super.messageCode,
  MessageKeys.songQueue.index : songQueue, MessageKeys.currentSongHash.index : currentSongHash,
  MessageKeys.clients.index : clients});

  @override
  factory RoomStatus.unpack(Uint8List packed){
    var d = deserialize(packed);
    return RoomStatus(d[MessageKeys.songQueue.index], d[MessageKeys.clients.index], d[MessageKeys.currentSongHash.index]);
  }
}

class SongDataConfirm extends Message{
  SongDataConfirm(this.receivedAll, this.wantedSongs) : super(MessageNum.SongDataConfirm.index);
  bool receivedAll;
  List<String> wantedSongs;//list of hashes.

  @override
  Uint8List pack() => serialize({MessageKeys.messageCode.index : super.messageCode,
  MessageKeys.receivedAll.index : receivedAll, MessageKeys.wantedSongs.index : wantedSongs});

  @override
  factory SongDataConfirm.unpack(Uint8List packed){
    var d = deserialize(packed);
    return SongDataConfirm(d[MessageKeys.receivedAll.index], d[MessageKeys.wantedSongs.index]);
  }
}

class SongData extends Message{
  SongData(this.sendTo, this.songHash) : super(MessageNum.SongData.index);
  List<String> sendTo;
  String songHash;

  @override
  Uint8List pack() => serialize({MessageKeys.messageCode.index : super.messageCode,
  MessageKeys.sendTo.index : sendTo, MessageKeys.songHash.index : songHash});

  @override
  factory SongData.unpack(Uint8List packed){
    var d = deserialize(packed);
    return SongData(d[MessageKeys.sendTo.index], d[MessageKeys.songHash.index]);
  }
}

class SongDataBytes extends Message{
  SongDataBytes(this.data) : super(MessageNum.SongData.index);
  Uint8List data;

  @override
  Uint8List pack() => serialize({MessageKeys.messageCode.index : super.messageCode,
  MessageKeys.songData.index : data});

  @override
  factory SongDataBytes.unpack(Uint8List packed){
    var d = deserialize(packed);
    return SongDataBytes(d[MessageKeys.songData.index]);
  }
}

class PlayData extends Message{
  PlayData(this.startAt, this.songHash) : super(MessageNum.PlayData.index);
  int startAt;
  DateTime get startAtDatetime{
    return DateTime.fromMillisecondsSinceEpoch(startAt);
  }
  String songHash;

  @override
  Uint8List pack() => serialize({MessageKeys.messageCode.index : super.messageCode,
  MessageKeys.startAt.index : startAt, MessageKeys.songHash.index : songHash});

  @override
  factory PlayData.unpack(Uint8List packed){
    var d = deserialize(packed);
    return PlayData(d[MessageKeys.startAt.index], d[MessageKeys.songHash.index]);
  }
}

class QuitRoom extends Message{
  QuitRoom() : super(MessageNum.QuitRoom.index);
  
  @override
  Uint8List pack() => super.pack();

  @override
  factory QuitRoom.unpack(Uint8List packed){
    return Message.unpack(packed);
  }
}
