import 'package:flutter/cupertino.dart';
import 'package:streamlouderapp/base/definitions.dart';
import 'package:streamlouderapp/base/general.dart';
class CreateRoom with ChangeNotifier{
  
  /// false by default, true when we need to transfer the manager to the room landing page.
  bool isReady;
  /// false by defualt, true when createRoom *Method* is passed the proper values(not empty room anme and password)
  bool stringsValid;
  bool clientsAmountValid;
  bool alert;

  CreateRoom(){
    _resetValues();
  }

  void _resetValues()
  {
    isReady = false;
    stringsValid = false;
    clientsAmountValid = false;
    alert = false;
  }

  @override
  void dispose(){
    super.dispose();
    //instance = null;
  }

  String roomName;
  String userName;
  String password;
  int maxClients;

  void createRoom(String roomName, String password, int maxClients, String userName){
    _resetValues();
    alert = true;
    try
    {
      if(password.isNotEmpty && roomName.isNotEmpty && userName.isNotEmpty)
      {
        stringsValid = true;
        if(maxClients >= ConstantValues.minimumMaxClients && maxClients <= ConstantValues.maximumMaxClients)
        {
          Communication.initializeUdpSocket();
          Communication.initializeTcpSocket(TcpSocketType.ManagerListenerSocket);
          clientsAmountValid = true;
          isReady = true;
          alert = false;
          this.roomName = roomName;
          this.password = password;
          this.maxClients = maxClients;
          this.userName = userName;
        }
      }
    }
    catch(e){print( "Create Room logic: " + e.toString());}
    notifyListeners();
  }
 
}