import 'package:flutter/cupertino.dart';
import 'package:streamlouderapp/base/definitions.dart';
import 'package:streamlouderapp/base/general.dart';
import 'package:streamlouderapp/models/dataStructs.dart' as dataStructs;

class JoinRoom with ChangeNotifier{

  /// static representation of the newest instance of this backend, to be notified when the data is received.
  static JoinRoom currentInstance;

  /// representation of the data we will receive about the room.
  dataStructs.GetRoomData roomData;

  bool isReady;
  bool alert;
  bool isLoading;
  String address;
  bool maxClientsReached;
  bool wrongPass;
  bool alreadyStarted;
  bool nameTaken;

  String userName;
  String password;

  JoinRoom(this.address){
    Communication.initializeTcpSocket(TcpSocketType.ClientSocket, ip: address);
    currentInstance = this;
    _clear();
  }

  /// Method is used to be aware that the client socket is set tnd that we now can send msgs.
  void clientIsSet(){
    Communication.sendMessage(dataStructs.Message(dataStructs.MessageNum.GetRoomData.index), TcpSocketType.ManagerListenerSocket);
  }

  @override
  void dispose() {
    super.dispose();
    currentInstance = null;
  }

  /// Used by [Communication] when wanting to alert.
  void alertListeners(){
    isLoading = false;
    if(isReady)
      Communication.requestTimeReSync();
    notifyListeners();
  }

  void _clear(){
    isReady = false;
    alert = false;
    isLoading = false;
    maxClientsReached = false;
    wrongPass = false;
    alreadyStarted = false;
  }

  void joinRoom(){
    alert = false;
    isLoading = false;
    if(userName.isNotEmpty && password.isNotEmpty)
    {
      Communication.sendMessage(dataStructs.JoinRoom(userName, password), TcpSocketType.ManagerListenerSocket);
      isLoading = true;
    }
    else
      alert = true;      
    notifyListeners();
  }
}