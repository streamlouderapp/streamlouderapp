import 'package:flutter/cupertino.dart';
import 'package:streamlouderapp/base/definitions.dart';
import 'package:streamlouderapp/base/general.dart';
import 'package:streamlouderapp/models/dataStructs.dart' as dataStructs;

class ClientRoomLandingPage with ChangeNotifier{
  static ClientRoomLandingPage instance;

  bool kicked;
  bool roomStart;
  dataStructs.GetRoomData roomData;
  String userName;
  dataStructs.RoomStatus roomStatus;

  ClientRoomLandingPage(this.roomData, this.userName){
    instance = this;
    kicked = false;
    roomStart = false;
  }
  
  @override
  void dispose(){
    super.dispose();
    instance = null;
  }

  void alertRoomStatusUpdate(){
    if(kicked){
      Communication.closeClient();
    }
    if(roomStart){
      Communication.sendMessage(dataStructs.RoomStart(), TcpSocketType.ManagerListenerSocket);
    }
    notifyListeners();
  }

  void quitRoom(){
    Communication.sendMessage(dataStructs.KickClient(), TcpSocketType.ManagerListenerSocket);
  }
}