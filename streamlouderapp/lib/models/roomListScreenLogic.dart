import 'package:flutter/cupertino.dart';
import 'package:streamlouderapp/base/general.dart';

class RoomListScreenLogic with ChangeNotifier{
  static RoomListScreenLogic instance;
  Map<String, String> get roomsList{
    return Communication.roomsList;
  }
  RoomListScreenLogic(){
    Communication.initializeUdpSocket();
    instance = this;
  }

  @override
  void dispose(){
    super.dispose();
    instance = null;
  }

  void alertRoomsListUpdated(){
    notifyListeners();
  }

  void refreshRoomList(){
    Communication.requestRefreshRoomsList();
    notifyListeners();
  }
}