import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:streamlouderapp/base/appConfig.dart';
import 'package:streamlouderapp/base/definitions.dart';
import 'package:streamlouderapp/models/joinRoom.dart' as joinRoom;
import 'package:streamlouderapp/pages/roomLandingPageClient.dart';
import '../base/general.dart';


class CustomAppBar extends StatefulWidget implements PreferredSizeWidget {
  CustomAppBar({Key key, this.title, this.leading}) : preferredSize = Size.fromHeight(kToolbarHeight), super(key: key);

  @override
  final Size preferredSize; // default is 56.0

  @override
  _CustomAppBarState createState() => _CustomAppBarState();

  final Widget leading;
  final String title;
}

class _CustomAppBarState extends State<CustomAppBar>{
  _CustomAppBarState();
  @override
  Widget build(BuildContext context) {
    return AppBar(iconTheme: IconThemeData(color: ConstantValues.seconderyBackgroundColor),
    centerTitle: true,
    title: Text(widget.title, textAlign: TextAlign.center, 
    style: TextStyle(color: ConstantValues.seconderyBackgroundColor, fontSize: ConstantValues.titleFontSize)),
    actions: <Widget>[ExtraMenuItems()],
    backgroundColor: ConstantValues.mainBackgroundColor, leading: widget.leading);
  }
}

class ExtraMenuItems extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    var style = TextStyle(color: ConstantValues.mainBackgroundColor);
    return PopupMenuButton(color: ConstantValues.seconderyBackgroundColor, itemBuilder: (BuildContext context) => <PopupMenuEntry<PopupMenuButtonOptions>>[
      PopupMenuItem<PopupMenuButtonOptions>(
      value: PopupMenuButtonOptions.TOS,
      child: Text(AppConfig.localizedStrings.tos(), style: style,),
    ),
      PopupMenuItem<PopupMenuButtonOptions>(
      value: PopupMenuButtonOptions.About,
      child: Text(AppConfig.localizedStrings.about(), style: style,),
    )],
    onSelected: (PopupMenuButtonOptions option){print(option.toString() + "selected");},
    onCanceled: (){print("cancled");},);
  }
}

class RoundFlatButton extends StatelessWidget{
  RoundFlatButton({this.splashColor = Colors.white, this.onPressed, this.text, this.fontSize = ConstantValues.defualtFontSize,
  this.btnBackgroundColor = Colors.blue, this.textColor = Colors.blue});
  final Color splashColor;
  final Function onPressed;
  final String text;
  final double fontSize;
  final Color btnBackgroundColor;
  final Color textColor;
  
  @override 
  Widget build(BuildContext context){
    return FlatButton(splashColor: splashColor,
            color: btnBackgroundColor,
            onPressed: onPressed != null ? onPressed : (){},
            shape: new RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(ConstantValues.roundRadius))),
            child: Text(text, textAlign: TextAlign.center,  
            style: TextStyle(color: textColor, fontSize: fontSize)));
  }
}

class CustomCircularProgressBar extends StatelessWidget{
  CustomCircularProgressBar({this.mainColor = false});
  final bool mainColor;
  @override
  Widget build(BuildContext context) {
    return Align(
        alignment: Alignment.center,
        child:
        CircularProgressIndicator(valueColor: AlwaysStoppedAnimation(!mainColor ? ConstantValues.mainBackgroundColor : ConstantValues.seconderyBackgroundColor),
        backgroundColor: mainColor ? ConstantValues.mainBackgroundColor : ConstantValues.seconderyBackgroundColor,)
        );
  } 
}

class KickUserPopupContent extends StatelessWidget{

  /// creates kick user popup content (should be used using the [Helper] to push the popup)
  KickUserPopupContent(this.username, this.address);

  final String username;
  final String address;
  @override
  Widget build(BuildContext context) {
    var lst = <Widget>[
    /*We should pop the view once we are done*/
    RoundFlatButton(btnBackgroundColor: ConstantValues.mainBackgroundColor,
      onPressed: (){Communication.kickClient(address); Navigator.of(context).pop();},
      fontSize: ConstantValues.defualtFontSize,
      splashColor: ConstantValues.mainBackgroundColor,
      textColor: ConstantValues.seconderyBackgroundColor,
      text: AppConfig.localizedStrings.yes()),
    Spacer(),
    RoundFlatButton(btnBackgroundColor: ConstantValues.mainBackgroundColor,
      onPressed: (){Navigator.of(context).pop();},
      fontSize: ConstantValues.defualtFontSize,
      splashColor: ConstantValues.mainBackgroundColor,
      textColor: ConstantValues.seconderyBackgroundColor,
      text: AppConfig.localizedStrings.no())
    ];
    if(address == ConstantValues.localLoopback.address){ // if the user is the manager we should not allow to kick.
      lst.removeAt(0);
      lst.removeAt(0);
    }
    return Directionality(textDirection: Helper.textDirection(), child:
    Container(decoration: BoxDecoration(color: ConstantValues.seconderyBackgroundColor),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, 
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(AppConfig.localizedStrings.kickUser() + " " + username + " ?", 
          style: TextStyle(color: ConstantValues.mainBackgroundColor, fontSize: ConstantValues.titleFontSize)),
        Row(crossAxisAlignment: CrossAxisAlignment.center, mainAxisAlignment: MainAxisAlignment.center,
          children: lst)
      ]),
    ));
  }
  
}

class JoinRoomPopupContent extends StatelessWidget{

  /// creates join room popup content (should be used using the [Helper] to push the popup)
  JoinRoomPopupContent(this.address);

  final String address;

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<joinRoom.JoinRoom>(child:
    Directionality(textDirection: Helper.textDirection(), child:
    Consumer(builder: (context, joinRoom.JoinRoom myModel, child) {
      if(myModel.alert)
      {
        String textForPopup;
        if(myModel.alreadyStarted)
          textForPopup = AppConfig.localizedStrings.partyAlreadyStarted();
        else if(myModel.wrongPass)
          textForPopup = AppConfig.localizedStrings.wrongPassword();
        else if(myModel.maxClientsReached)
          textForPopup = AppConfig.localizedStrings.maxClientsReached();
        else if(myModel.nameTaken)
          textForPopup = AppConfig.localizedStrings.nameTaken();
        else
          textForPopup = AppConfig.localizedStrings.stringAreInvalid();
        Future.delayed(Duration.zero, (){Helper.pushDialog(context, AppConfig.localizedStrings.notice(), textForPopup, 
        actions: [ButtonsAndAction(context, AppConfig.localizedStrings.understood())]);});
      }
      else if(myModel.isReady){
        Future.delayed(Duration.zero, (){
          while(Navigator.canPop(context)){Navigator.pop(context);}// pop till we are @ Rooms List Page
          Navigator.push(context, MaterialPageRoute(builder: (context) => RoomLandingPageClient(myModel.roomData, myModel.userName)));
        });
      }
      var border = OutlineInputBorder(
        borderSide: const BorderSide(color: ConstantValues.mainBackgroundColor, width: ConstantValues.roundBorderWidth),
        borderRadius: BorderRadius.circular(ConstantValues.roundRadius)
      );
      var margin = EdgeInsets.all(AppConfig.screenWidth * ConstantValues.applicationWidthMarginPercentage);
      var style = TextStyle(color: ConstantValues.mainBackgroundColor);
      return myModel.roomData == null || myModel.isLoading ?
        CustomCircularProgressBar(mainColor: true) :
        Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(myModel.roomData.roomName, style: TextStyle(color: ConstantValues.mainBackgroundColor, 
          fontSize: ConstantValues.titleFontSize), textAlign: TextAlign.center),
          Text(myModel.roomData.currentClientsAmount.toString() +
          "/" + myModel.roomData.amountOfMaxClients.toString() + " " + 
          AppConfig.localizedStrings.connectedUsers(), style: TextStyle(color: ConstantValues.mainBackgroundColor, 
          fontSize: ConstantValues.titleFontSize), textAlign: TextAlign.start),
          Container(child: TextFormField(onChanged: (String changed){myModel.userName = changed;},
            cursorColor: ConstantValues.mainBackgroundColor,
            maxLength: ConstantValues.maxStringLength,
            decoration: InputDecoration(
            counterStyle: TextStyle(color: ConstantValues.mainBackgroundColor),
            hintStyle: style,
            hintText: AppConfig.localizedStrings.yourName(),
            fillColor: ConstantValues.mainBackgroundColor,
            hoverColor: ConstantValues.mainBackgroundColor,
            focusedBorder: border,
            enabledBorder: border), style: style), margin: margin),
          Container(child: TextFormField(onChanged: (String changed){myModel.password = changed;},
            cursorColor: ConstantValues.mainBackgroundColor,
            obscureText: true,
            maxLength: ConstantValues.maxStringLength,
            decoration: InputDecoration(
            counterStyle: TextStyle(color: ConstantValues.mainBackgroundColor),
            hintStyle: style,
            hintText: AppConfig.localizedStrings.roomPassword(),
            fillColor: ConstantValues.mainBackgroundColor,
            hoverColor: ConstantValues.mainBackgroundColor,
            focusedBorder: border,
            enabledBorder: border), style: style,), margin: margin),
          RoundFlatButton(btnBackgroundColor: ConstantValues.mainBackgroundColor,
            onPressed: (){myModel.joinRoom();},
            fontSize: ConstantValues.defualtFontSize,
            splashColor: ConstantValues.mainBackgroundColor,
            textColor: ConstantValues.seconderyBackgroundColor,
            text: AppConfig.localizedStrings.join())
        ]);
    })),
    create: (ctxt)=> joinRoom.JoinRoom(address));
  }
  
}