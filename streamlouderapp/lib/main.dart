import 'package:flutter/material.dart';
import 'package:streamlouderapp/Pages/roomsList.dart';
import 'package:streamlouderapp/base/definitions.dart';
import 'package:streamlouderapp/base/communicationIsolate.dart' as communicationIsolate;
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:streamlouderapp/base/general.dart';

void main() async {await communicationIsolate.Communicationisolate.callerCreateIsolate(); runApp(MyApp());}

class MyApp extends StatefulWidget {
  static _MyAppState _state;
  @override
  State<MyApp> createState() {
    _state = _MyAppState();
    return _state;
  }
  static void restart(){
    _state._restart();
  }
}
class _MyAppState extends State<MyApp>{
  
  void _restart(){
    setState(() {
      
    });
  }

  @override
  void dispose() {
    Communication.requestIsolateShutdown();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
     return new DynamicTheme(
      defaultBrightness: Brightness.light,
      data: (brightness) => new ThemeData(
        primarySwatch: ConstantValues.mainColorSwatch,
        cursorColor: ConstantValues.seconderyBackgroundColor,
        primaryColorLight: ConstantValues.seconderyBackgroundColorLight,
        primaryColorDark: ConstantValues.seconderyBackgroundColorDark,
        brightness: brightness,
      ),
      themedWidgetBuilder: (context, theme) {
        return MaterialApp(
          title: 'Flutter Demo',
          theme: theme,
          home: RoomsListPage());
        });
  }

}